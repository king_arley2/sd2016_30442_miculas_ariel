package com.example;

import com.example.business.PatientException;
import com.example.business.PatientProcessor;
import com.example.database.Client;
import com.example.database.ClientForm;
import com.example.database.ClientRepository;
import com.example.security.ClientType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ClinicApplication.class)
@WebAppConfiguration
public class ClinicApplicationTests {

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private PatientProcessor patientProcessor;

	@Test
	public void contextLoads() {
	}

	@Test
	public void populateDb() {
		String user = "user";
		ClientForm clientForm = new ClientForm("Ionel", user, user, ClientType.Admin);
		Client client = new Client(clientForm);
		if (clientRepository.findByUsername(user).size() == 0)
		{
			clientRepository.save(client);
		}
		assert (clientRepository.findByUsername(user).size() == 1);
	}

	@Test
	public void validCnpTest()
	{
		try {
			patientProcessor.validateControlDigit("2810820670482");
		}
		catch (PatientException e)
		{
			assert (false);
			e.printStackTrace();
		}
	}

	@Test(expected = PatientException.class)
	public void invalidCnpTest() throws PatientException {
		patientProcessor.validateControlDigit("2810820670483");
	}

}
