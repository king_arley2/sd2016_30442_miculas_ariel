package com.example.database;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

/**
 * Created by Ary on 23-Apr-16.
 */
//patient information: name, identity card number, personal numerical code, date of birth, address).
@Entity
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String identityCardNumber;
    @Column(unique = true, nullable = false)
    private String personalNumericalCode;
    @Column(nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime dateOfBirth;
    @Column(nullable = false)
    private String address;

    public Patient() {
    }
    public Patient(PatientForm patientForm)
    {
        setFields(patientForm);
    }

    public void setFields(PatientForm patientForm)
    {
        this.name = patientForm.getName();
        this.identityCardNumber = patientForm.getIdentityCardNumber();
        this.personalNumericalCode = patientForm.getPersonalNumericalCode();
        this.dateOfBirth = patientForm.getDateOfBirth();
        this.address = patientForm.getAddress();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setIdentityCardNumber(String identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }

    public String getPersonalNumericalCode() {
        return personalNumericalCode;
    }

    public void setPersonalNumericalCode(String personalNumericalCode) {
        this.personalNumericalCode = personalNumericalCode;
    }

    public DateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(DateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
