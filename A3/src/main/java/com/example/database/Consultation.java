package com.example.database;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.persistence.*;

/**
 * Created by Ary on 5/6/2016.
 */
@Entity
public class Consultation {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @ManyToOne(optional = false)
    @JoinColumn(name = "patient", referencedColumnName = "id")
    private Patient patient;

    @ManyToOne(optional = false)
    @JoinColumn(name = "client", referencedColumnName = "id")
    private Client doctor;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime consultationDate;

    private String details;

    public Consultation()
    {

    }

    public Consultation(ConsultationForm consultationForm, Patient patient, Client doctor)
    {
        setFields(consultationForm, patient, doctor);
    }

    public void setFields(ConsultationForm consultationForm, Patient patient, Client doctor)
    {
        this.patient = patient;
        this.doctor = doctor;
        consultationDate = consultationForm.getConsultationDate();
        details = consultationForm.getDetails();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Client getDoctor() {
        return doctor;
    }

    public void setDoctor(Client client) {
        this.doctor = client;
    }

    public DateTime getConsultationDate() {
        return consultationDate;
    }

    public void setConsultationDate(DateTime consultationDate) {
        this.consultationDate = consultationDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {

        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd/yyyy");

        return "Consultation{" +
                "id=" + id +
                ", patient=" + patient.getName() +
                ", doctor=" + doctor.getName() +
                ", consultationDate=" + dtfOut.print(consultationDate) +
                ", details='" + details + '\'' +
                '}';
    }
}
