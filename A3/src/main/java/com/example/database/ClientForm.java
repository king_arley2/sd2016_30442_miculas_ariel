package com.example.database;

import com.example.security.ClientType;

import javax.validation.constraints.Pattern;

/**
 * Created by Ary on 23-Apr-16.
 */
public class ClientForm {
    private Integer id;

    @Pattern(regexp = "[a-zA-Z' .-]+")
    private String name;

    @Pattern(regexp = "[a-zA-Z0-9_.-]+")
    private String username;

//    This regex will enforce these rules (or matches the empty string):
//
//    At least one upper case english letter
//    At least one lower case english letter
//    At least one digit
//    At least one special character
//    Minimum 8 in length
    @Pattern(regexp = "(^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%^&*()-]).{8,}$)|(^$)")
    private String password;
    private ClientType type;

    public ClientForm() {
    }

    public ClientForm(String name, String username, String password, ClientType type) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public ClientForm(Client client)
    {
        id = client.getId();
        name = client.getName();
        username = client.getUsername();
        type = client.getType();
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ClientType getType() {
        return type;
    }

    public void setType(ClientType type) {
        this.type = type;
    }
}
