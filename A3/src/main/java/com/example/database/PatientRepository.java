package com.example.database;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Ary on 23-Apr-16.
 */
public interface PatientRepository extends JpaRepository<Patient, Integer> {
    List<Patient> findByPersonalNumericalCode(String personalNumbericalCode);
    List<Patient> findByName(String name);
    List<Patient> findByIdentityCardNumber(String identityCardNumber);
}
