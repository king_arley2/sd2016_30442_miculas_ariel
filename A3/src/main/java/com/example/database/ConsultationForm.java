package com.example.database;

import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;

/**
 * Created by Ary on 5/6/2016.
 */
public class ConsultationForm {

    private Integer id;
    @NotNull
    private Integer idPatient;
    @NotNull
    private Integer idDoctor;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private DateTime consultationDate;
    private String details;

    public ConsultationForm()
    {

    }

    public ConsultationForm(Consultation consultation)
    {
        idPatient = consultation.getPatient().getId();
        idDoctor = consultation.getDoctor().getId();
        consultationDate = consultation.getConsultationDate();
        details = consultation.getDetails();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Integer idDoctor) {
        this.idDoctor = idDoctor;
    }

    public DateTime getConsultationDate() {
        return consultationDate;
    }

    public void setConsultationDate(DateTime consultationDate) {
        this.consultationDate = consultationDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
