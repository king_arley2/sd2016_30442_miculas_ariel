package com.example.database;

import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Created by Ary on 02-May-16.
 */
public class PatientForm {
    private Integer id;

    @Pattern(regexp = "[a-zA-Z' .-]+")
    private String name;

    @Pattern(regexp = "^\\d{6}$", message = "seria contine 6 cifre!")
    private String identityCardNumber;

    @Pattern(regexp = "^[1-9]\\d{2}" +
            "((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])" +
            "|((04|06|09|11)(0[1-9]|[1-2][0-9]|30))" +
            "|((02)(0[1-9]|[1-2][0-9])))" +
            "(0[1-9]|[1-3][0-9]|4[0-6]|5[1-2])" +
            "\\d{4}$")
    private String personalNumericalCode;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private DateTime dateOfBirth;

    @Pattern(regexp = "^[a-zA-Z0-9.,' -]+$")
    private String address;

    public PatientForm() {
    }

    public PatientForm(Patient patient)
    {
        this.id = patient.getId();
        this.name = patient.getName();
        this.identityCardNumber = patient.getIdentityCardNumber();
        this.personalNumericalCode = patient.getPersonalNumericalCode();
        this.dateOfBirth = patient.getDateOfBirth();
        this.address = patient.getAddress();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setIdentityCardNumber(String identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }

    public String getPersonalNumericalCode() {
        return personalNumericalCode;
    }

    public void setPersonalNumericalCode(String personalNumericalCode) {
        this.personalNumericalCode = personalNumericalCode;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public DateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(DateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "PatientForm{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", identityCardNumber='" + identityCardNumber + '\'' +
                ", personalNumericalCode='" + personalNumericalCode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", address='" + address + '\'' +
                '}';
    }
}
