package com.example.database;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ary on 5/6/2016.
 */
public interface ConsultationRepository extends JpaRepository<Consultation, Integer> {
}
