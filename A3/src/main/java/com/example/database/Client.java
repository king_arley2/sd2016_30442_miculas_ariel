package com.example.database;

import com.example.security.ClientType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;

/**
 * Created by Ary on 23-Apr-16.
 */
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private String name;
    @Column(unique = true, nullable = false)
    private String username;
    @Column(nullable = false)
    private String passwordHash;
    @Column(nullable = false)
    private ClientType type;

    public Client() {
    }

    public Client(ClientForm clientForm)
    {
        setFields(clientForm);
    }

    public void setFields(ClientForm clientForm)
    {
        this.name = clientForm.getName();
        this.username = clientForm.getUsername();
        if (!clientForm.getPassword().isEmpty())
        {
            this.passwordHash =  new BCryptPasswordEncoder().encode(clientForm.getPassword());
        }
        this.type = clientForm.getType();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public ClientType getType() {
        return type;
    }

    public void setType(ClientType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", type=" + type +
                '}';
    }
}
