package com.example.database;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Ary on 23-Apr-16.
 */
public interface ClientRepository extends JpaRepository<Client, Integer>{
    List<Client> findByUsername(String username);
}
