package com.example.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * Created by Ary on 03-May-16.
 */
@Controller
public class DoctorController {
    @Autowired
    private SimpMessagingTemplate template;

    @RequestMapping("/doctor")
    public String seeUpdates()
    {
        return "doctor/doctor";
    }
}
