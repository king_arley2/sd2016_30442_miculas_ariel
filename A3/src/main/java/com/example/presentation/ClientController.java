package com.example.presentation;

import com.example.business.ClientException;
import com.example.business.ClientProcessor;
import com.example.database.Client;
import com.example.database.ClientForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Ary on 23-Apr-16.
 */
@Controller
@RequestMapping("/client")

public class ClientController {
    @Autowired
    private ClientProcessor clientProcessor;

    @RequestMapping("view")
    public String getClients(Model model)
    {
        List<Client> clientList = clientProcessor.findAll();
        model.addAttribute("clientList", clientList);
        return "client/viewClients";
    }

    @RequestMapping("/{clientId}")
    public String showClientInfo(@PathVariable("clientId") Integer clientId, Model model) {

        Client client = clientProcessor.getClient(clientId);
        if (client == null)
        {
            return "redirect:/404";
        }
//        System.out.println("user: " + client);

        model.addAttribute("client", client);
        return "client/clientDetails";
    }

    @RequestMapping(value="new", method = RequestMethod.GET)
    public String showFormCreateNewClient(ClientForm clientForm)
    {
        return "client/createClient";
    }

    @RequestMapping(value="new", method = RequestMethod.POST)
    public String createNewClient(@Valid ClientForm clientForm, BindingResult bindingResult, Model model)
    {
        Integer cliendId = null;
        if (bindingResult.hasErrors())
        {
            return "client/createClient";
        }

        try {
            cliendId = clientProcessor.save(clientForm);
        } catch (ClientException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "client/createClient";
        }

        return "redirect:/client/" + cliendId;
    }

    @RequestMapping(value = "/{clientId}/edit", method = RequestMethod.GET)
    public String processUpdateClientForm(@PathVariable("clientId") Integer clientId, Model model) {

        if (null == clientProcessor.getClient(clientId))
        {
            return "redirect:/404";
        }
        model.addAttribute("clientForm", clientProcessor.getClientForm(clientId));
        return "client/createClient";
    }

    @RequestMapping(value="/{clientId}/edit", method = RequestMethod.POST)
    public String updateClient(@PathVariable("clientId") Integer clientId,
                               @Valid ClientForm client,
                               BindingResult bindingResult,
                               Model model)
    {
        if (null == clientProcessor.getClient(clientId))
        {
            return "redirect:/404";
        }

        if (bindingResult.hasErrors())
        {
            return "client/createClient";
        }

        try {
            clientProcessor.update(clientId, client);
        } catch (ClientException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "client/createClient";
        }

        return "redirect:/client/" + clientId;
    }

    @RequestMapping(value = "/{clientId}/delete")
    public String processDeleteClientForm(@PathVariable("clientId") Integer clientId) {
        clientProcessor.delete(clientId);
        return "redirect:/client/view";
    }
}
