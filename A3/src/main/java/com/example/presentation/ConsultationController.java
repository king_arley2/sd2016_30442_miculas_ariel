package com.example.presentation;

import com.example.business.ClientProcessor;
import com.example.business.ConsultationProcessor;
import com.example.business.PatientProcessor;
import com.example.database.Consultation;
import com.example.database.ConsultationForm;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Created by Ary on 5/6/2016.
 */
@Controller
@RequestMapping("/consultation")
public class ConsultationController {

    @Autowired
    private ConsultationProcessor consultationProcessor;

    @Autowired
    private PatientProcessor patientProcessor;

    @Autowired
    private ClientProcessor clientProcessor;

    @Autowired
    private SimpMessagingTemplate template;

    @RequestMapping("view")
    public String getConsultations(Model model)
    {
        List<Consultation> consultationList = consultationProcessor.findAll();
        model.addAttribute("consultationList", consultationList);
        return "consultation/viewConsultations";
    }

    @RequestMapping("/{consultationId}")
    public String showConsultationInfo(@PathVariable("consultationId") Integer consultationId, Model model) {

        Consultation consultation = consultationProcessor.getConsultation(consultationId);
        if (consultation == null)
        {
            return "redirect:/404";
        }
        System.out.println("consultation: " + consultation);

        model.addAttribute("consultation", consultation);
        return "consultation/consultationDetails";
    }

    @RequestMapping(value="new", method = RequestMethod.GET)
    public String showFormCreateNewConsultation(ConsultationForm consultationForm, Model model)
    {
        model.addAttribute("patientList", patientProcessor.findAll());
        model.addAttribute("doctorList", clientProcessor.findAllDoctors());
        return "consultation/createConsultation";
    }

    @RequestMapping(value="new", method = RequestMethod.POST)
    public String createNewConsultation(@Valid ConsultationForm consultationForm, BindingResult bindingResult, Model model)
    {
        Integer consultationId = null;
        model.addAttribute("patientList", patientProcessor.findAll());
        model.addAttribute("doctorList", clientProcessor.findAllDoctors());
        if (bindingResult.hasErrors())
        {
            System.out.println("ceva eroare!" + bindingResult.toString());
            return "consultation/createConsultation";
        }
        consultationId = consultationProcessor.save(consultationForm);

        return "redirect:/consultation/" + consultationId;
    }

    @RequestMapping(value = "/{consultationId}/edit", method = RequestMethod.GET)
    public String processUpdateConsultationForm(@PathVariable("consultationId") Integer consultationId, Model model) {
        if (null == consultationProcessor.getConsultation(consultationId))
        {
            return "redirect:/404";
        }
        model.addAttribute("patientList", patientProcessor.findAll());
        model.addAttribute("doctorList", clientProcessor.findAllDoctors());
        model.addAttribute("consultationForm", consultationProcessor.getConsultationForm(consultationId));
        return "consultation/createConsultation";
    }

    @RequestMapping(value="/{consultationId}/edit", method = RequestMethod.POST)
    public String updateConsultation(@PathVariable("consultationId") Integer consultationId,
                                     @Valid ConsultationForm consultationForm,
                                     BindingResult bindingResult,
                                     Model model)
    {
        if (null == consultationProcessor.getConsultation(consultationId))
        {
            return "redirect:/404";
        }
        model.addAttribute("patientList", patientProcessor.findAll());
        model.addAttribute("doctorList", clientProcessor.findAllDoctors());

        if (bindingResult.hasErrors())
        {
            return "consultation/createConsultation";
        }
        consultationProcessor.update(consultationId, consultationForm);
        return "redirect:/consultation/" + consultationId;
    }

    @RequestMapping(value = "/{consultationId}/delete")
    public String processDeleteConsultationForm(@PathVariable("consultationId") Integer consultationId) {
        consultationProcessor.delete(consultationId);
        return "redirect:/consultation/view";
    }

    @RequestMapping(value = "/{consultationId}/inform")
    public String processInform(@PathVariable("consultationId") Integer consultationId)
    {
        DateTime now = new DateTime();
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd HH:mm");

        template.convertAndSend("/topic/message", dtfOut.print(now) + "  " + consultationProcessor.getConsultation(consultationId));
        return "redirect:/consultation/" + consultationId;
    }
}
