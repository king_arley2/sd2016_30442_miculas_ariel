package com.example.presentation;

import com.example.business.PatientException;
import com.example.business.PatientProcessor;
import com.example.database.Patient;
import com.example.database.PatientForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Ary on 02-May-16.
 */
@Controller
@RequestMapping("/patient")
public class PatientController {
    @Autowired
    private PatientProcessor patientProcessor;

    @RequestMapping("view")
    public String getPatients(Model model)
    {
        List<Patient> patientList = patientProcessor.findAll();
        model.addAttribute("patientList", patientList);
        return "patient/viewPatients";
    }

    @RequestMapping("/{patientId}")
    public String showPatientInfo(@PathVariable("patientId") Integer patientId, Model model) {

        Patient patient = patientProcessor.getPatient(patientId);
        if (patient == null)
        {
            return "redirect:/404";
        }
        System.out.println("patient: " + patient);

        model.addAttribute("patient", patient);
        return "patient/patientDetails";
    }

    @RequestMapping(value="new", method = RequestMethod.GET)
    public String showFormCreateNewPatient(PatientForm patientForm)
    {
        return "patient/createPatient";
    }

    @RequestMapping(value="new", method = RequestMethod.POST)
    public String createNewPatient(@Valid PatientForm patientForm, BindingResult bindingResult, Model model)
    {
        Integer patientId = null;
        if (bindingResult.hasErrors())
        {
            return "patient/createPatient";
        }

        try {
            patientId = patientProcessor.save(patientForm);
        } catch (PatientException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "patient/createPatient";
        }

        return "redirect:/patient/" + patientId;
    }

    @RequestMapping(value = "/{patientId}/edit", method = RequestMethod.GET)
    public String processUpdatePatientForm(@PathVariable("patientId") Integer patientId, Model model) {

        if (null == patientProcessor.getPatient(patientId))
        {
            return "redirect:/404";
        }
        model.addAttribute("patientForm", patientProcessor.getPatientForm(patientId));
        return "patient/createPatient";
    }

    @RequestMapping(value="/{patientId}/edit", method = RequestMethod.POST)
    public String updatePatient(@PathVariable("patientId") Integer patientId,
                               @Valid PatientForm patientForm,
                               BindingResult bindingResult,
                               Model model)
    {
        if (null == patientProcessor.getPatient(patientId))
        {
            return "redirect:/404";
        }

        if (bindingResult.hasErrors())
        {
            return "patient/createPatient";
        }

        try {
            patientProcessor.update(patientId, patientForm);
        } catch (PatientException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "patient/createPatient";
        }

        return "redirect:/patient/" + patientId;
    }

    @RequestMapping(value = "/{patientId}/delete")
    public String processDeletePatientForm(@PathVariable("patientId") Integer patientId) {
        patientProcessor.delete(patientId);
        return "redirect:/patient/view";
    }
}
