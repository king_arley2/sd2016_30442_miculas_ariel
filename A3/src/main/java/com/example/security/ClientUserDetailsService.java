package com.example.security;


import com.example.database.Client;
import com.example.database.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by Ary on 16-Apr-16.
 */
@Component
public class ClientUserDetailsService implements UserDetailsService{

    @Autowired
    ClientRepository clientRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ArrayList<Client> clientArrayList = new ArrayList<Client>(clientRepository.findByUsername(username));
        if (clientArrayList.size()!=1)
        {
            throw new UsernameNotFoundException("UserName "+username+" not found");
        }

        Client client = clientArrayList.get(0);
        SecurityClient securityClient = new SecurityClient(client);
        return securityClient;
    }
}
