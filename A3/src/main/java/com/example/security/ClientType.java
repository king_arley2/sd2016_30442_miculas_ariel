package com.example.security;

/**
 * Created by Ary on 17-Apr-16.
 */
public enum ClientType {
    Secretary,
    Doctor,
    Admin
}
