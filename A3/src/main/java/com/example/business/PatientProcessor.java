package com.example.business;

import com.example.database.Patient;
import com.example.database.PatientForm;
import com.example.database.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 23-Apr-16.
 */
@Component
public class PatientProcessor {
    @Autowired
    private PatientRepository patientRepository;

    public List<Patient> findAll(){
        return patientRepository.findAll();
    }

    public Patient getPatient(Integer id)
    {
        return patientRepository.findOne(id);
    }

    public void validateControlDigit(String pnc) throws PatientException {
        int i;
        int sum = 0;
        int controlDigit;
        String controlString = "279146358279";
        for (i = 0; i < pnc.length() - 1; i++)
        {
            sum += Character.getNumericValue(pnc.charAt(i)) * Character.getNumericValue(controlString.charAt(i));
        }
        if (sum % 11 == 10)
        {
            controlDigit = 1;
        }
        else
        {
            controlDigit = sum % 11;
        }
        if (controlDigit != Character.getNumericValue(pnc.charAt(i)))
        {
            throw new PatientException("Invalid control digit in PNC!");
        }
    }

    public void validateCnp(String pnc, Patient currentPatient) throws PatientException {
        checkForDuplicateCnp(pnc, currentPatient);
        validateControlDigit(pnc);
    }

    public void checkForDuplicateCnp(String pnc, Patient currentPatient) throws PatientException {
        List<Patient> patientList = null;
        patientList = patientRepository.findByPersonalNumericalCode(pnc);

        if (patientList.size() > 0)
        {
            if (currentPatient == null)
            {
                throw new PatientException("PNC already in use");
            }
            else
            {
                if (patientList.size() == 1)
                {
                    System.out.println(patientList.size());
                    System.out.println(currentPatient.getPersonalNumericalCode());
                    System.out.println(patientList.get(0).getPersonalNumericalCode());
                    if (!currentPatient.getPersonalNumericalCode().equals(patientList.get(0).getPersonalNumericalCode()))
                    {
                        throw new PatientException("PNC already in use");
                    }
                }
                else {
                    throw new PatientException("PNC already in use");
                }
            }
        }
    }

    public Integer save(PatientForm patientForm) throws PatientException {
        validateCnp(patientForm.getPersonalNumericalCode(), null);
        Patient patient = new Patient(patientForm);
        patientRepository.save(patient);
        return patient.getId();
    }

    public PatientForm getPatientForm(Integer idPatient)
    {
        return new PatientForm(getPatient(idPatient));
    }

    public void update(Integer idPatient, PatientForm patientForm) throws PatientException {
        Patient patient = getPatient(idPatient);
        if (patient == null)
        {
            throw new PatientException("patient id " + idPatient + " was not found!");
        }
        validateCnp(patientForm.getPersonalNumericalCode(), patient);

        patient.setFields(patientForm);
        patientRepository.save(patient);
    }

    public void delete(Integer id)
    {
        Patient patient = getPatient(id);
        if (patient != null) {
            patientRepository.delete(patient);
        }
    }
}
