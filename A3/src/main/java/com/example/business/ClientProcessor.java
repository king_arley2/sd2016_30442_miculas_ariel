package com.example.business;

import com.example.database.Client;
import com.example.database.ClientForm;
import com.example.database.ClientRepository;
import com.example.security.ClientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 23-Apr-16.
 */
@Component
public class ClientProcessor {
    @Autowired
    ClientRepository clientRepository;

    public List<Client> findAll()
    {
        return clientRepository.findAll();
    }

    public List<Client> findAllDoctors()
    {
        List<Client> clientList = findAll();
        clientList.removeIf(s -> s.getType() != ClientType.Doctor);
        return clientList;
    }

    public Client getClient(Integer id)
    {
        return clientRepository.findOne(id);
    }

    public void checkForDuplicateUsername(String username, Client currentClient) throws ClientException {
        List<Client> clientList = null;
        clientList = clientRepository.findByUsername(username);

        if (clientList.size() > 0)
        {
            if (currentClient == null)
            {
                throw new ClientException("Username already in use");
            }
            else
            {
                if (clientList.size() == 1)
                {
                    if (!currentClient.getUsername().equals(clientList.get(0).getUsername()))
                    {
                        throw new ClientException("Username already in use");
                    }
                }
                else {
                    throw new ClientException("Username already in use");
                }
            }
        }
    }

    public Integer save(ClientForm clientForm) throws ClientException {
        checkForDuplicateUsername(clientForm.getUsername(), null);
        if (clientForm.getPassword().isEmpty())
        {
            throw new ClientException("password field is empty!");
        }
        Client client = new Client(clientForm);
        clientRepository.save(client);
        return client.getId();
    }

    public ClientForm getClientForm(Integer idClient)
    {
        return new ClientForm(getClient(idClient));
    }

    public void update(Integer idClient, ClientForm clientForm) throws ClientException {
        Client client = getClient(idClient);
        if (client == null)
        {
            throw new ClientException("client id " + idClient + " was not found!");
        }

        checkForDuplicateUsername(clientForm.getUsername(), client);
        client.setFields(clientForm);
        clientRepository.save(client);
    }

    public void delete(Integer id)
    {
        Client client = getClient(id);
        if (client != null) {
            clientRepository.delete(client);
        }
    }
}
