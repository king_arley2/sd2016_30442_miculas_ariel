package com.example.business;

import com.example.database.*;
import com.example.security.ClientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 5/6/2016.
 */
@Component
public class ConsultationProcessor {
    @Autowired
    private ConsultationRepository consultationRepository;

    @Autowired
    private ClientProcessor clientProcessor;

    @Autowired
    private PatientProcessor patientProcessor;

    public List<Consultation> findAll()
    {
        return consultationRepository.findAll();
    }

    public Consultation getConsultation(Integer id)
    {
        return consultationRepository.findOne(id);
    }

    public Client getDoctor(ConsultationForm consultationForm) throws ClientException {
        Client doctor = clientProcessor.getClient(consultationForm.getIdDoctor());
        if (doctor.getType() != ClientType.Doctor)
        {
            throw new ClientException("the client is not a doctor!");
        }
        return doctor;
    }

    public Patient getPatient(ConsultationForm consultationForm)
    {
        return patientProcessor.getPatient(consultationForm.getIdPatient());

    }

    public ConsultationForm getConsultationForm(Integer idConsultation)
    {
        return new ConsultationForm(getConsultation(idConsultation));
    }

    public Integer save(ConsultationForm consultationForm)
    {
        Consultation consultation = null;
        try {
            consultation = new Consultation(consultationForm,
                    getPatient(consultationForm),
                    getDoctor(consultationForm));
        } catch (ClientException e) {
            e.printStackTrace();
        }
        consultationRepository.save(consultation);
        return consultation.getId();
    }

    public void update(Integer idConsultation, ConsultationForm consultationForm)
    {
        Consultation consultation = getConsultation(idConsultation);
        if (consultation == null)
        {
            System.out.println("Consultation not found!");
        }
        else
        {
            try {
                consultation.setFields(consultationForm,
                        getPatient(consultationForm),
                        getDoctor(consultationForm));
            } catch (ClientException e) {
                e.printStackTrace();
            }
            consultationRepository.save(consultation);
        }
    }

    public void delete(Integer idConsultation)
    {
        Consultation consultation = getConsultation(idConsultation);
        if (consultation != null)
        {
            consultationRepository.delete(consultation);
        }
    }
}
