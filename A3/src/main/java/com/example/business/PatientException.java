package com.example.business;

/**
 * Created by Ary on 02-May-16.
 */
public class PatientException extends Exception {
    public PatientException(String message) {
        super(message);
    }
}
