package com.example.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class LoginController {

    @RequestMapping("/login")
    public String greeting() {
        return "login";
    }

    @RequestMapping("/home")
    public String home(String name, Model model)
    {
        return "home";
    }


    @RequestMapping("/hello")
    public String hello(String name, Model model)
    {
        return "hello";
    }

    @RequestMapping("/success")
    public String success(String name, Model model)
    {
        return "success";
    }

    @RequestMapping("/")
    public String index(String name, Model model)
    {
        return "home";
    }
}
