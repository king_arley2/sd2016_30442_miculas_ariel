package com.example.presentation;

import com.example.business.ProcessReport;
import com.example.business.ReportFactory;
import com.example.database.DateIntervalForm;
import com.example.database.Report;
import com.example.database.ReportType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Controller
public class ReportController {
    @Autowired
    private ProcessReport processReport;
    @Autowired
    private ReportFactory reportFactory;
    private List<Report> reportList;

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public String generateReportGet(DateIntervalForm dateIntervalForm)
    {
        return "report/report";
    }

    @RequestMapping(value = "/report", method = RequestMethod.POST)
    public String generateReportPost(@Valid DateIntervalForm dateIntervalForm, BindingResult bindingResult, Model model)
    {
        reportList = processReport.findAllBetweenDate(dateIntervalForm);
        model.addAttribute("reportList", reportList);
        return "report/report";
    }

    @RequestMapping(value = "/report/download/{type}")
    @ResponseBody
    public FileSystemResource downloadReportPdf(@PathVariable("type") ReportType type)
    {
        String path =  reportFactory.getReportType(type, reportList).generateReport();
        return new FileSystemResource(path);
    }
}
