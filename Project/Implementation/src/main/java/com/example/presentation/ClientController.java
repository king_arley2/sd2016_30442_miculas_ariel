package com.example.presentation;

import com.example.business.ClientException;
import com.example.business.ProcessClient;
import com.example.database.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Ary on 05-Apr-16.
 */
@RequestMapping("/client")
@Controller
public class ClientController {
    @Autowired
    private ProcessClient processClient;

    @RequestMapping("view")
    public String getClients(Model model)
    {
        List<Client> list;
        list = processClient.findAll();
        model.addAttribute("clientList", list);
        return "client/viewClients";
    }

    @RequestMapping(value="new", method = RequestMethod.GET)
    public String showFormCreateNewClient(Client client)
    {
        return "client/createClient";
    }

    @RequestMapping(value="new", method = RequestMethod.POST)
    public String createNewClient(@Valid Client client, BindingResult bindingResult, Model model)
    {
        if (bindingResult.hasErrors())
        {
            return "client/createClient";
        }

        try {
            processClient.saveClient(client);
        } catch (ClientException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "client/createClient";
        }

        return "redirect:/client/" + client.getIdClient();
    }

    @RequestMapping("/{clientId}")
    public String showClientInfo(@PathVariable("clientId") int clientId, Model model) {
        Client client = processClient.findOne(clientId);
        model.addAttribute("client", client);
        return "client/clientDetails";
    }

    @RequestMapping(value = "/{clientId}/edit", method = RequestMethod.GET)
    public String processUpdateClientForm(@PathVariable("clientId") int clientId, Model model) {
        Client client = processClient.findOne(clientId);
        model.addAttribute("client", client);
        return "client/createClient";
    }

    @RequestMapping(value="/{clientId}/edit", method = RequestMethod.POST)
    public String updateClient(@PathVariable("clientId") int clientId,
                               @Valid Client client,
                               BindingResult bindingResult,
                               Model model)
    {
        if (bindingResult.hasErrors())
        {
            return "client/createClient";
        }

        try {
            processClient.updateClient(clientId, client);
        } catch (ClientException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "client/createClient";
        }

        return "redirect:/client/" + clientId;
    }

    @RequestMapping(value = "/{clientId}/delete")
    public String processDeleteClientForm(@PathVariable("clientId") int clientId) {
        processClient.deleteClient(clientId);
        return "redirect:/client/view";
    }
}
