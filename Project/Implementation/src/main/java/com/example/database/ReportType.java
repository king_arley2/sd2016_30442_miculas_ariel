package com.example.database;

/**
 * Created by Ary on 17-Apr-16.
 */
public enum ReportType {
    PdfReport,
    CsvReport
}
