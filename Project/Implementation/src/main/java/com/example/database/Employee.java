package com.example.database;

import com.example.security.UserType;
import org.hibernate.validator.constraints.NotBlank;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

/**
 * Created by Ary on 04-Apr-16.
 */
@Entity
public class Employee{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer idEmployee;
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z]+([a-zA-Z]|-|\\s)*$")
    private String name;
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9_-]+$")
    private String username;
//    This regex will enforce these rules (or matches the empty string):
//
//    At least one upper case english letter
//    At least one lower case english letter
//    At least one digit
//    At least one special character
//    Minimum 8 in length
    @Pattern(regexp = "(^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%^&*()-]).{8,}$)|(^$)")
    private String passwordHash;
    private UserType type;

    public void setFields(Employee employee)
    {
        name = employee.getName();
        username = employee.getUsername();
        if (!employee.getPasswordHash().isEmpty())
        {
            passwordHash = employee.getPasswordHash();
        }
        type = employee.getType();
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "idEmployee=" + idEmployee +
                ", name='" + name + '\'' +
                ", username=" + username +
                ", password='" + passwordHash + '\'' +
                ", type=" + type +
                '}';
    }
}
