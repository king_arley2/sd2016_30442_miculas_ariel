package com.example.database;

/**
 * Created by Ary on 04-Apr-16.
 */
public enum AccountType {
    SavingAccount,
    SpendingAccount
}
