package com.example.database;

import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by Ary on 5/26/2016.
 */

public class DateIntervalForm {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private DateTime startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private DateTime endDate;

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }
}
