package com.example.database;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import java.util.Collection;

/**
 * Created by Ary on 04-Apr-16.
 */

//Add/update/view client information (name, identity card number, personal numerical code, address, etc.).

@Entity
public class Client {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer idClient;
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z]+([a-zA-Z]|-|\\s)*$")
    private String name;
    @Pattern(regexp = "^\\d{6}$")
    private String identityCardNumber;
    @NotBlank
    @Column(unique = true)
    @Pattern(regexp = "^[1-9]\\d{2}" +
            "((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])" +
            "|((04|06|09|11)(0[1-9]|[1-2][0-9]|30))" +
            "|((02)(0[1-9]|[1-2][0-9])))" +
            "\\d{6}$")
    private String personalNumericalCode;
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9.,' -]+$")
    private String address;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private Collection<Account> accounts;


    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setIdentityCardNumber(String identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }

    public String getPersonalNumericalCode() {
        return personalNumericalCode;
    }

    public void setPersonalNumericalCode(String personalNumericalCode) {
        this.personalNumericalCode = personalNumericalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Collection<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Collection<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "Client{" +
                "idClient=" + idClient +
                ", name='" + name + '\'' +
                ", identityCardNumber='" + identityCardNumber + '\'' +
                ", personalNumericalCode='" + personalNumericalCode + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
