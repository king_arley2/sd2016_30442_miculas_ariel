package com.example.business;

import com.example.database.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created by Ary on 06-Apr-16.
 */
@Component
public class ProcessAccount {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ProcessClient processClient;
    @Autowired
    private ProcessReport processReport;


    public Account findOne(Integer idAccount)
    {
        return accountRepository.findOne(idAccount);
    }

    public void transferMoney(Account sourceAccount,
                              Integer destinationAccountId,
                              Integer amount)
            throws AccountException
    {
        Integer sourceMoney = sourceAccount.getAmount();
        Integer destinationMoney;

        Account destinationAccount = accountRepository.findOne(destinationAccountId);
        if (null == destinationAccount)
        {
            throw new AccountException("Account does not exist!");
        }

        if (destinationAccount.getIdAccount() == sourceAccount.getIdAccount())
        {
            throw new AccountException("Cannot transfer to your own account!");
        }

        destinationMoney = destinationAccount.getAmount();

        if (sourceMoney < amount)
        {
            throw new AccountException("Not enough money!");
        }
        sourceMoney -= amount;
        destinationMoney += amount;
        sourceAccount.setAmount(sourceMoney);
        destinationAccount.setAmount(destinationMoney);
        accountRepository.save(destinationAccount);
        accountRepository.save(sourceAccount);
        System.out.println("Transfering money");
        Report report = new Report("transfer", sourceAccount, destinationAccount.getIdAccount(), amount);
        processReport.save(report);
    }

    public void saveAccount(Integer clientId,
                            Account account)
    {
        Client client = processClient.findOne(clientId);
        //client.getAccounts().add(account);
        account.setClient(client);
        accountRepository.save(account);
        System.out.println("Saving account");
        Report report = new Report("create", account);
        processReport.save(report);
    }

    public void updateAccount(Account account,
                              Integer accountId,
                              Integer clientId)
    {
        account.setIdAccount(accountId);
        account.setClient(processClient.findOne(clientId));
        accountRepository.save(account);
        System.out.println("Updating account");
        //Report report = new Report("update", account);
        Report report = new Report("update", account);
        processReport.save(report);
    }

    public void deleteAccount(Integer accountId)
    {
        accountRepository.delete(accountId);
        System.out.println("Deleting account");
    }
}
