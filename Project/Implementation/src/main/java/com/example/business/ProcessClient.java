package com.example.business;

import com.example.database.Client;
import com.example.database.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Component
public class ProcessClient {

    @Autowired
    ClientRepository clientRepository;
    public Client findOne(Integer idClient)
    {
        return clientRepository.findOne(idClient);
    }

    public List<Client> findAll()
    {
        return clientRepository.findAll();
    }

    public List<Client> findByPersonalNumericalCode(String pnc)
    {
        return clientRepository.findByPersonalNumericalCode(pnc);
    }

    public void validateControlDigit(String pnc) throws ClientException {
        int i;
        int sum = 0;
        int controlDigit;
        String controlString = "279146358279";
        for (i = 0; i < pnc.length() - 1; i++)
        {
            sum += Character.getNumericValue(pnc.charAt(i)) * Character.getNumericValue(controlString.charAt(i));
        }
        if (sum % 11 == 10)
        {
            controlDigit = 1;
        }
        else
        {
            controlDigit = sum % 11;
        }
        if (controlDigit != Character.getNumericValue(pnc.charAt(i)))
        {
            throw new ClientException("Invalid control digit in PNC!");
        }
    }

    public void validateCnp(String pnc, Client currentClient) throws ClientException {
        checkForDuplicateCnp(pnc, currentClient);
        validateControlDigit(pnc);
    }

    public void checkForDuplicateCnp(String pnc, Client currentClient) throws ClientException {
        List<Client> clientList = null;
        clientList = clientRepository.findByPersonalNumericalCode(pnc);

        if (clientList.size() > 0)
        {
            if (currentClient == null)
            {
                throw new ClientException("PNC already in use");
            }
            else
            {
                if (clientList.size() == 1)
                {
                    System.out.println(clientList.size());
                    System.out.println(currentClient.getPersonalNumericalCode());
                    System.out.println(clientList.get(0).getPersonalNumericalCode());
                    if (!currentClient.getPersonalNumericalCode().equals(clientList.get(0).getPersonalNumericalCode()))
                    {
                        throw new ClientException("PNC already in use");
                    }
                }
                else {
                    throw new ClientException("PNC already in use");
                }
            }
        }
    }

    public void updateClient(Integer clientId, Client client) throws ClientException
    {
        Client currentClient = findOne(clientId);
        validateCnp(client.getPersonalNumericalCode(), currentClient);
        client.setIdClient(clientId);
        clientRepository.save(client);
        System.out.println("Updating client");
    }

    public void saveClient(Client client) throws ClientException
    {
        validateCnp(client.getPersonalNumericalCode(), null);
        clientRepository.save(client);
        System.out.println("Saving client");
    }

    public void deleteClient(Integer clientId)
    {
        clientRepository.delete(clientId);
        System.out.println("Deleting client");
    }
}
