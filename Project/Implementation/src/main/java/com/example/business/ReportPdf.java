package com.example.business;

import com.example.database.Report;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.awt.print.Book;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


/**
 * Created by Ary on 17-Apr-16.
 */
public class ReportPdf implements ReportInterface{
    private java.util.List<Report> reportList;
    public ReportPdf(java.util.List<Report> reportList) {
        this.reportList = reportList;
    }

    public String generateReport() {

        Document document = new Document();
        final String path = "reports/statement.pdf";

        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream(path));

            document.open();

            PdfPTable table = new PdfPTable(6);

            float[] columnWidths = {1f, 4f, 3f, 2f, 2f, 2f};

            table.setWidths(columnWidths);
            table.addCell(new Paragraph("Id"));
            table.addCell(new Paragraph("Message"));
            table.addCell(new Paragraph("Date"));
            table.addCell(new Paragraph("Source account id"));
            table.addCell(new Paragraph("Destination account id"));
            table.addCell(new Paragraph("Amount transfered"));

            if (reportList != null)
            {
                for (Report report : reportList) {
                    table.addCell(new Paragraph(report.getIdReport().toString()));
                    table.addCell(new Paragraph(report.getMessage().toString()));
                    table.addCell(new Paragraph(report.getDateOfCreation().toString()));
                    table.addCell(new Paragraph(report.getSourceAccount().getIdAccount().toString()));
                    table.addCell(new Paragraph(report.getDestinationAccountId().toString()));
                    table.addCell(new Paragraph(report.getTransferAmount().toString()));
                }
            }
            document.add(table);
            document.close();
            return path;
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
