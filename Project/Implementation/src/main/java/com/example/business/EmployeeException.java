package com.example.business;

/**
 * Created by Ary on 5/26/2016.
 */
public class EmployeeException extends Exception {
    public EmployeeException(String message) {
        super(message);
    }
}
