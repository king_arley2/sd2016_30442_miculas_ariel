package com.example.business;

import com.example.database.Employee;
import com.example.database.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Component
public class ProcessEmployee {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> findAll()
    {
        return employeeRepository.findAll();
    }

    public Employee findOne(Integer id)
    {
        return employeeRepository.findOne(id);
    }
    public List<Employee> findByUsername(String username)
    {
        return employeeRepository.findByUsername(username);
    }

    public void delete(Integer id)
    {
        employeeRepository.delete(id);
    }

    public void checkForDuplicateUsername(String username, Employee currentEmployee) throws EmployeeException {
        List<Employee> employeeList = null;
        employeeList = employeeRepository.findByUsername(username);

        if (employeeList.size() > 0)
        {
            if (currentEmployee == null)
            {
                throw new EmployeeException("Username already in use");
            }
            else
            {
                if (employeeList.size() == 1)
                {
                    if (!currentEmployee.getUsername().equals(employeeList.get(0).getUsername()))
                    {
                        throw new EmployeeException("Username already in use");
                    }
                }
                else {
                    throw new EmployeeException("Username already in use");
                }
            }
        }
    }

    public void save(Employee employee) throws EmployeeException {
        checkForDuplicateUsername(employee.getUsername(), null);
        if (employee.getPasswordHash().isEmpty())
        {
            throw new EmployeeException("The password field cannot be empty!");
        }
        employee.setPasswordHash(new BCryptPasswordEncoder().encode(employee.getPasswordHash()));
        employeeRepository.save(employee);
    }

    public void update(Integer id, Employee employee) throws EmployeeException {
        Employee existingEmployee = findOne(id);
        if (existingEmployee == null)
        {
            throw new EmployeeException("client id " + id + " was not found!");
        }

        checkForDuplicateUsername(employee.getUsername(), existingEmployee);

        if (!employee.getPasswordHash().isEmpty())
        {
            employee.setPasswordHash(new BCryptPasswordEncoder().encode(employee.getPasswordHash()));
        }
        existingEmployee.setFields(employee);
        employeeRepository.save(existingEmployee);
    }
}
