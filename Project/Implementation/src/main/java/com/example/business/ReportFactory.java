package com.example.business;

import com.example.database.Report;
import com.example.database.ReportType;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 17-Apr-16.
 */
@Component
public class ReportFactory {

    public ReportInterface getReportType(ReportType type, List<Report> reportList)
    {
        if (type.equals(ReportType.CsvReport))
        {
            return new ReportCsv(reportList);
        }
        else if (type.equals(ReportType.PdfReport))
        {
            return new ReportPdf(reportList);
        }
        else
        {
            return null;
        }
    }
}
