package com.example.business;

import com.example.database.Report;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Ary on 17-Apr-16.
 */
public class ReportCsv implements ReportInterface{
    private List<Report> reportList;
    public ReportCsv(List<Report> reportList) {
        this.reportList = reportList;
    }

    public String generateReport()
    {
        final String path = "reports/statement.csv";
        try {
            FileWriter fileWriter = new FileWriter(path);

            for (Report report : reportList) {
                    fileWriter.append(report.toStringCsv());
            }
            fileWriter.flush();
            fileWriter.close();
            return path;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
