package com.example.business;

import com.example.database.DateIntervalForm;
import com.example.database.Report;
import com.example.database.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Component
public class ProcessReport {
    @Autowired
    private ReportRepository reportRepository;

    public List<Report> findAll()
    {
        return reportRepository.findAll();
    }

    public List<Report> findAllBetweenDate(DateIntervalForm dateIntervalForm)
    {
        List<Report> all = findAll();
        all.removeIf(s -> s.getDateOfCreation().compareTo(dateIntervalForm.getStartDate()) < 0);
        all.removeIf(s -> s.getDateOfCreation().compareTo(dateIntervalForm.getEndDate()) > 0);
        return all;
    }

    public void save(Report report)
    {
        reportRepository.save(report);
    }
}
