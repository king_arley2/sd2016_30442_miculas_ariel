package com.example.business;

/**
 * Created by Ary on 5/26/2016.
 */
public class ClientException extends Exception {
    public ClientException(String message) {
        super(message);
    }
}
