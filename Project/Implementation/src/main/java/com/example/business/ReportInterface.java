package com.example.business;

/**
 * Created by Ary on 17-Apr-16.
 */
public interface ReportInterface {
    public String generateReport();
}
