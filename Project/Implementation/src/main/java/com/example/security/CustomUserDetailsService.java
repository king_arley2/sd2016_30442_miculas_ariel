package com.example.security;

import com.example.business.ProcessEmployee;
import com.example.database.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 11-Apr-16.
 */
@Component
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private ProcessEmployee processEmployee;

    @Override
    public UserDetails loadUserByUsername(String userName)
    {
        System.out.println("Trying to authenticate user: " + userName);
        List<Employee> users = processEmployee.findByUsername(userName);

        if (users.size() == 1) {
            Employee user = users.get(0);
            SecurityEmployee securityEmployee = new SecurityEmployee(user);
            System.out.println("authority: " + securityEmployee.getAuthorities().toString());
            return securityEmployee;
        }
        else {
            throw new UsernameNotFoundException("UserName "+userName+" not found");
        }
    }
}
