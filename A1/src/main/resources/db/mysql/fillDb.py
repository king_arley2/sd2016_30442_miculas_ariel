#!/usr/bin/python
import sys
import random
import string
import bcrypt
from random import randint


random.seed()
addresses = ["4 Glenside, Castle Bytham, Grantham, Lincolnshire NG33 4SS, UK", "25 St John\\'s St, Wirksworth, Matlock, Derbyshire DE4 4DR, UK", "Bunhill Fields Burial And Gardens, London EC1Y 8ND, UK", "22 Featherbank Terrace, Horsforth, Leeds, West Yorkshire LS18 4QW, UK", "18 Beddington Cross, Croydon, Greater London CR0, UK", "5 Pitt Dr, Seaford, East Sussex BN25 3JB, UK", "A9, Kingussie, Highland PH21, UK", "A3082, Par, Cornwall PL24, UK", "60 Spring Gardens, Manchester, Manchester M60 1HU, UK", "6 Half Acre Ln, Beaminster, Dorset DT8 3DJ, UK", "14 Cockerell Rise, East Cowes, Isle of Wight PO32 6JL, UK", "5 Rhubana View, Morar, Mallaig, Highland PH40 4PF, UK", "11 Puddledock, London EC4V 3PD, UK", "15 Hazlemere Rd, South Benfleet, Benfleet, Essex SS7 4AF, UK", "12A Commercial Rd, Poole, Poole BH14 0JW, UK", "Wellow Drove, West Wellow, Romsey, Hampshire SO51 6DU, UK", "7 Walton St, Atherton, Manchester M46 9RF, UK", "17 Downies Rd, Aberdeen, Aberdeenshire AB12 4QX, UK", "5 Beech Hill Rd, Sutton Coldfield, West Midlands B72 1DU, UK", "2 Arran Cl, Sandy, Central Bedfordshire SG19 1QN, UK", "B6342, Newcastle upon Tyne, Northumberland NE19 2BE, UK", "3 Earl\\'s Ct, Fifth Ave, Team Valley Trading Estate, Gateshead, Tyne and Wear NE11 0HF, UK", "Rowland Hill Cl, Swindon, Swindon SN3, UK", "23 Queen Square, London WC1N 3BG, UK", "Larkins Ln, Much Hadham, Hertfordshire SG10 6AJ, UK", "A470, Dolwyddelan, Conwy LL25, UK", "Portland St, Llanon, Ceredigion SY23 5HF, UK", "4 Barrett Rd, Darlington, Darlington DL3 8LB, UK", "6 Wannock Ave, Eastbourne, East Sussex BN20 9RS, UK", "12 Target Cl, Stroud, Gloucestershire GL5 1JA, UK", "1 Caspian Square, Brighton, The City of Brighton and Hove BN2 7LE, UK", "15 Bridge St, Louth, Lincolnshire LN11 0DR, UK", "1A Churchill Cl, Thornton-Cleveleys, Lancashire FY5 4BQ, UK", "Fisherman\\'s Wharf, Grimsby, North East Lincolnshire DN31 1SY, UK", "218-236 A579, Bolton, Bolton BL3, UK", "15 Walmsley Dr, Upton, Pontefract, West Yorkshire WF9 1JW, UK", "83 Sorrell Dr, Newport Pagnell, Milton Keynes MK16 8TZ, UK", "1 Rudgate Park, Thorp Arch, Wetherby, West Yorkshire LS23 7RB, UK", "Unnamed Road, Tywyn, Gwynedd LL36 9TL, UK", "2 Katrine Ave, Dundee, Dundee City DD5 3HD, UK", "Unnamed Road, Newquay, Cornwall TR8, UK", "18 Salisbury St, Stockport, Stockport SK5 6UL, UK", "1 Raspberry Cottages, High Hunsley, Cottingham, East Riding of Yorkshire HU20 3UR, UK", "3 Straiton Dr, Hamilton, South Lanarkshire ML3 9ET, UK", "29 Clementhorpe, York, York YO23 1PN, UK", "3 Trinity Square, Llandudno, Conwy LL30 2PY, UK", "13-15 Royal Sovereign Ave, Gillingham, Chatham, Medway ME4 4UW, UK", "Patmore Rd, Stansted, Essex CM24, UK", "20 Elm St, Errol, Perth, Perth and Kinross PH2 7SQ, UK", "193 Goldfinch Rd, Poole, Poole BH17 7TB, UK", "111-123 Crampton St, London SE17 3AA, UK", "1 Cowdenhill Gardens, Bonnybridge, Falkirk FK4 1AQ, UK", "1 Lower Kelly, Calstock, Cornwall PL18 9SA, UK", "11 Smith House Ave, Brighouse, West Yorkshire HD6 2LE, UK", "10 Westmorland Rd, Partington, Manchester M31 4WT, UK", "Turner Rd, Glasgow, Glasgow City G21, UK", "A30, Salisbury, Wiltshire SP3 4BG, UK", "1 Grassington Cres, Liverpool, Merseyside L25 9RU, UK", "22 Squire\\'s Bridge Rd, Shepperton, Surrey TW17 0LB, UK", "2 Wellfield Ct, Willen, Milton Keynes, Milton Keynes MK15 9HL, UK", "A1090, Grays, Essex RM20, UK", "7 Gleadless Rise, Sheffield, South Yorkshire S12 2UW, UK", "37 Bedford St, Leamington Spa, Warwickshire CV32 5DY, UK", "Sheepcote Grange, Bromsgrove, Worcestershire B61, UK", "3 Greenslade Rd, Totnes, Devon TQ9 7PB, UK", "Beech Rd, Yorkley, Lydney, Gloucestershire GL15 4TJ, UK", "Viscount St, London EC1Y 0SA, UK", "1 Walsh Dr, Sutton Coldfield, West Midlands B76 2NS, UK", "24 Queen\\'s Rd, Stoke-on-Trent, Stoke-on-Trent ST4 7LJ, UK", "21 Donich Park, Lochgoilhead, Cairndow, Argyll and Bute PA24 8AB, UK", "B266, Thornton Heath, Greater London CR7 8RX, UK", "Unnamed Road, Llangadog, Carmarthenshire SA19, UK", "23D Nellfield Pl, Aberdeen, Aberdeen City AB10 6DE, UK", "A38, Winscombe, North Somerset BS25 1NL, UK", "Main Rd, Northampton, Northamptonshire NN6 7UD, UK", "70 St Martin\\'s Ln, London WC2N 4JS, UK", "43 Gurnos Rd, Ystalyfera, Swansea, Neath Port Talbot SA9 2JA, UK", "108 Old Greenock Rd, Bishopton, Renfrewshire PA7 5BB, UK", "29 High St, Tain, Highland IV19 1AE, UK", "27 Glebe Rd, Galston, East Ayrshire KA4, UK", "A882, Halkirk, Highland KW12 6UZ, UK", "5 Pipeland Rd, St Andrews, Fife KY16 8JN, UK", "7 Bracara, Morar, Mallaig, Highland PH40 4PE, UK", "A591, Windermere, Cumbria LA23, UK", "The Causeway, Buntingford, Hertfordshire SG9 0JN, UK", "5-19 Cwmbeth Cl, Crickhowell, Powys NP8 1DX, UK", "1 Clownholme Cottages, Marston Bank, Rocester, Uttoxeter, Derbyshire ST14 5BP, UK", "4 Love Ln, Sandbach, Cheshire East CW11 2TH, UK", "214 A50, Uttoxeter, Staffordshire ST14, UK", "28 The Headlands, Coventry, West Midlands CV5 8HA, UK", "Unnamed Road, Falmouth, Cornwall TR11 5PJ, UK", "2 Wynn Rd, Wolverhampton, West Midlands WV4 4AL, UK", "6 Falahill Cottages, Heriot, Scottish Borders EH38 5YG, UK", "3 Town Head, Bootle, Millom, Cumbria LA19 5TH, UK", "74 Drivers Mead, Lingfield, Surrey RH7 6EX, UK", "A4061, Bryncethin, Bridgend, Bridgend CF32 9TA, UK", "Hazel Bank, Cairndow, Argyll and Bute PA25 8BA, UK", "38 Colinmill Rd, Dunmurry, Belfast, Lisburn BT17 0AP, UK", "Corporation St, Saint Helens, Merseyside WA10, UK", "20-28 Lichfield Rd, Northwood, Greater London HA6, UK"]

firstNames = [
			"Ana", "Maria", "Vasile",  "Aida", "Alex", "Vlad", "Ioan", "Cristi", "Ionut", "Laura", "Roxana", "Iulia", "Simona","Sorina", "Andu", "Tudor", "Stefan", 
			"Tony", "Granville", "Darell", "Curt", "Stephan", "Zachary", "Neville", "Dewitt", "Wiley", "Jed", "Elbert", "Francisco", "Jerrell", "Reid", "Ross", "Christoper", "Jewell", "Jasper", "Clement", "Jeff", "Marlon", "Trevor", "Jimmie", "Hung", "Jc", "Russ", "Faustino", "Josh", "Valentin", "Ahmed", "Bertram", "Milton", "Nickolas", "Brenton", "Ervin", "Franklyn", "Micheal", "Brad", "Norbert", "Alfredo", "Gordon", "Ramiro", "Daniel", "Emmanuel", "Bobbie", "Bart", "Buddy", "Philip", "Francesco", "Lloyd"
			"Destiny", "Noemi", "Jin", "Jeffie", "Zena", "Monnie", "Christinia", "Raguel", "Ricarda", "Shawna", "Bao", "Roxie", "Katie", "Karan", "Pearl", "Aurora", "Della", "Aleen", "Jenifer", "Daisey", "Regena", "Zelda", "Mi", "Louann", "Tona", "Kathi", "Venessa", "Lottie", "Bethanie", "Armanda", "Annalee", "Macie", "Blanca", "Beverley", "Corazon", "Belle", "Trang", "Armida", "Ailene", "Nichole", "Fe", "Maryann", "Madge", "Marielle", "Jule", "Kourtney", "Stephenie", "Dede", "Ilona", "Tobi"
			]
lastNames = ["Pop", "Avarvarei", "Popescu", "Petrescu", "Vanta", "Micle", "Emiloiu", "Constantinescu", "Corde"
			,"Billings", "Mehan", "Dilorenzo", "Honeycutt", "Hamed", "Crooms", "Berthiaume", "Segawa", "Gelb", "Vancleve", "Guerrera", "Zuk", "Towell", "Simpson", "Herder", "Segarra", "Triolo", "Cairo", "Muller", "Yang", "Bartlow", "Wooldridge", "Stipe", "Peirce", "Smelser", "Bynum", "Pennington", "Estelle", "Priddy", "Basch", "Lewison", "Mccants", "Fortes", "Diggins", "Goldenberg", "Levitsky", "Gowdy", "Mckendree", "Ohler", "Whobrey", "Deeb", "Santee", "Scheel", "Hirata", "Francisco", "Rakestraw", "Guerro", "Weitzman", "Purdie", "Totman"
			"Weinstock", "Corvin", "Seeman", "Cotnoir", "True", "Yon", "Paille", "Satterwhite", "Kintzel", "Friel", "Shull", "Friedt", "Sedor", "Hallum", "Mifflin", "Butner", "Agustin", "Deshazo", "Days", "Stano", "Bost", "Weidman", "Delaune", "Moss", "Christ", "Gramling", "Kash", "Ballew", "Bunde", "Shupp", "Beeks", "Langenfeld", "Archambeault", "Ried", "Easley", "Kestner", "Dockstader", "Gorsuch", "Spikes", "Parrinello", "Raffa", "Picou", "Oxner", "Lomax", "Tosi", "Cast", "Henery", "Dement", "Cullinan", "Tobin"
			]

def populateAccounts(Number):
	for i in xrange(Number):
		id, amount, type, client = i + 1, randint(0,1000000), randint(0,1), randint(1,nrOfClients)
		date = "%.4d-%.2d-%.2d %.2d:%.2d:%.2d" % (randint(2000,2020), randint(0,12), randint(0,28),
					randint(0,23), randint(0,59), randint(0,59))

		print "INSERT IGNORE INTO `bank`.`account` VALUES('%d', '%s', '%s', '%d', '%d');" % (id, amount, date, type, client)
def populateClients(NumberOfClients):
	for i in xrange(NumberOfClients):
		id = i + 1
		address = addresses[randint(0, len(addresses) - 1)] + " " + str(randint(1,999))
		identitycard = "%.6d" % randint(0,999999)
		name = lastNames[randint(0, len(lastNames) - 1)] + " " + firstNames[randint(0, len(firstNames) - 1)]
		cnp = "%.1d%.2d%.2d%.2d%.6d" % (randint(1,2), randint(0,99),randint(1,12), randint(1,29), randint(0,999999))
		print "INSERT IGNORE INTO `bank`.`client` VALUES('%d', '%s', '%s', '%s', '%s');" % (id, address, identitycard, name, cnp)
def populateEmployees(NumberOfEmployees):
	for i in xrange(NumberOfEmployees):
		id = i + 1
		lastName = lastNames[randint(0, len(lastNames) - 1)]
		firstName = firstNames[randint(0, len(firstNames) - 1)]
		name = lastName + " " + firstName
		username = firstName + "_" + lastName + "_" + str(randint(1,999))
		password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(randint(8,10)))
		hashed = bcrypt.hashpw(password, bcrypt.gensalt())
		hashed = hashed[:2] + 'a' + hashed[3:] #replace b with a, java seems to have a problem with this
		type = randint(0,1)
		out_file.write('id %d username %s password %s\n' % (id, username, password))
		print "INSERT IGNORE INTO `bank`.`employee` VALUES('%d', '%s', '%s', '%s', '%s');" % (id, name, hashed, type, username)
		
if len(sys.argv) < 1 + 3:
	print "please enter <number of clients> <number of accounts> <number of employees>!"
	sys.exit()
	
out_file = open('passwords.txt', 'w')
nrOfClients = int(sys.argv[1])
nrOfAccounts = int(sys.argv[2])
nrOfEmployees = int(sys.argv[3])

populateClients(nrOfClients)
populateAccounts(nrOfAccounts)
populateEmployees(nrOfEmployees)