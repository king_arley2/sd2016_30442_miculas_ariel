package com.example.Presentation;

import com.example.Business.ProcessReport;
import com.example.Database.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Controller
public class ReportController {
    @Autowired
    private ProcessReport processReport;

    @RequestMapping(value = "/report")
    public String generateReport(Model model)
    {
        List<Report> list;
        list = processReport.findAll();
        model.addAttribute("reportList", list);
        return "report";
    }
}
