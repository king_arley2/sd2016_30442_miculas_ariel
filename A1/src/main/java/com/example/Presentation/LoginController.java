package com.example.Presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;


@Controller
public class LoginController {

    @RequestMapping("/login")
    public String greeting() {
        return "login";
    }

    @RequestMapping("/home")
    public String home(String name, Model model)
    {
        return "home";
    }


    @RequestMapping("/hello")
    public String hello(String name, Model model)
    {
        return "hello";
    }

    @RequestMapping("/success")
    public String success(String name, Model model)
    {
        return "success";
    }

    @RequestMapping("/")
    public String index(String name, Model model)
    {
        return "home";
    }

    // for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    @ResponseBody
    public String accesssDenied(Principal user) {
        String message;
        if (user != null) {
            message = "Hi " + user.getName()
                    + ", you do not have permission to access this page!";
        } else {
            message = "You do not have permission to access this page!";
        }

        return message;
    }
}
