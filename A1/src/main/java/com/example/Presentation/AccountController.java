package com.example.Presentation;

import com.example.Business.AccountException;
import com.example.Business.ProcessAccount;
import com.example.Database.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by Ary on 06-Apr-16.
 */
@RequestMapping("/client")
@Controller
public class AccountController {
    @Autowired
    ProcessAccount processAccount;

    @RequestMapping(value="/{clientId}/account/new", method= RequestMethod.GET)
    public String showFormCreateAccount(@PathVariable("clientId") int clientId, Account account, Model model)
    {
        return "createAccount";
    }

    @RequestMapping(value = "/{clientId}/account/new", method = RequestMethod.POST)
    public String createAccount(@PathVariable("clientId") int clientId, @Valid Account account, BindingResult bindingResult, Model model)
    {
        if (bindingResult.hasErrors()) {
            return "createAccount";
        }

        processAccount.saveAccount(clientId, account);
        return "redirect:/client/" + clientId + "/account/" + account.getIdAccount();
    }

    @RequestMapping(value = "/{clientId}/account/{accountId}", method = RequestMethod.GET)
    public String processViewAccount(@PathVariable("clientId") int clientId,
                                     @PathVariable("accountId") int accountId,
                                     Account account,
                                     Model model) {

        Account sourceAccount = processAccount.findOne(accountId);
        model.addAttribute("sourceAccount", sourceAccount);
        return "accountDetails";
    }

    @RequestMapping(value = "/{clientId}/account/{accountId}", method = RequestMethod.POST)
    public String ViewAccount(@PathVariable("clientId") int clientId,
                              @PathVariable("accountId") int accountId,
                              @Valid Account account,
                              BindingResult bindingResult,
                              Model model)
    {
        Account sourceAccount = processAccount.findOne(accountId);
        model.addAttribute("sourceAccount", sourceAccount);
        if (bindingResult.hasErrors())
        {
            return "accountDetails";
        }

        try {
            processAccount.transferMoney(sourceAccount, account.getIdAccount(), account.getAmount());
        } catch (AccountException e) {
            //account.setAmount(sourceAccount.getAmount());
            System.out.println(e);
            model.addAttribute("errorMessage", e.getMessage());
            return "accountDetails";
        }

        return "redirect:/client/" + clientId + "/account/" + accountId;
    }


    @RequestMapping(value = "/{clientId}/account/{accountId}/edit", method = RequestMethod.GET)
    public String processUpdateAccountForm(@PathVariable("clientId") int clientId,
                                           @PathVariable("accountId") int accountId,
                                           Model model) {
        Account account = processAccount.findOne(accountId);
        model.addAttribute("account", account);

        return "createAccount";
    }

    @RequestMapping(value="/{clientId}/account/{accountId}/edit", method = RequestMethod.POST)
    public String updateAccount(@PathVariable("clientId") int clientId,
                                @PathVariable("accountId") int accountId,
                               @Valid Account account,
                               BindingResult bindingResult,
                               Model model)
    {
        if (bindingResult.hasErrors())
        {
            return "createAccount";
        }

        processAccount.updateAccount(account, accountId, clientId);
        //System.out.println("account: " + account);


        return "redirect:/client/" + clientId + "/account/" + accountId;
    }


    @RequestMapping(value = "/{clientId}/account/{accountId}/delete", method = RequestMethod.GET)
    public String processDeleteAccount(@PathVariable("clientId") int clientId,
                                       @PathVariable("accountId") int accountId,
                                       Model model) {
        processAccount.deleteAccount(accountId);
        return "redirect:/client/" + clientId;
    }
}
