package com.example.Presentation;

import com.example.Business.ProcessEmployee;
import com.example.Database.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Ary on 05-Apr-16.
 */
@RequestMapping("/employee")
@Controller
public class EmployeeController {
    @Autowired
    private ProcessEmployee processEmployee;

    @RequestMapping(value="/new", method= RequestMethod.GET)
    public String showFormEmployee(Employee employee)
    {
        return "createEmployee";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String createEmployee(@Valid Employee employee, BindingResult bindingResult, Model model)
    {
        if (bindingResult.hasErrors()) {
            return "createEmployee";
        }

        processEmployee.save(employee);

        return "redirect:/employee/" + employee.getIdEmployee();
    }

    @RequestMapping("view")
    public String getEmployees(Model model)
    {
        List<Employee> list;
        list = processEmployee.findAll();
        model.addAttribute("employeeList", list);
        return "viewEmployees";
    }

    @RequestMapping("/{employeeId}")
    public String showEmployeeInfo(@PathVariable("employeeId") int employeeId, Model model) {
        Employee employee = processEmployee.findOne(employeeId);
        model.addAttribute("emp", employee);
        return "employeeDetails";
    }

    @RequestMapping(value = "/{employeeId}/edit", method = RequestMethod.GET)
    public String processUpdateEmployeeForm(@PathVariable("employeeId") int employeeId, Model model) {
        Employee employee = processEmployee.findOne(employeeId);
        model.addAttribute("employee", employee);
        return "createEmployee";
    }

    @RequestMapping(value="/{employeeId}/edit", method = RequestMethod.POST)
    public String updateEmployee(@PathVariable("employeeId") int employeeId,
                               @Valid Employee employee,
                               BindingResult bindingResult,
                               Model model)
    {
        if (bindingResult.hasErrors())
        {
            return "createEmployee";
        }

        employee.setIdEmployee(employeeId);
        processEmployee.save(employee);

        return "redirect:/employee/" + employeeId;
    }

    @RequestMapping(value = "/{employeeId}/delete")
    public String processDeleteClientForm(@PathVariable("employeeId") int employeeId) {
        processEmployee.delete(employeeId);
        return "redirect:/employee/view";
    }
}
