package com.example.Database;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Ary on 04-Apr-16.
 */
public interface ClientRepository extends JpaRepository<Client, Integer>{
    public List<Client> findByPersonalNumericalCode(String personalNumbericalCode);
    public List<Client> findByName(String name);
    public List<Client> findByIdentityCardNumber(String identityCardNumber);
}
