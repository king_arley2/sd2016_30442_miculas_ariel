package com.example.Database;

/**
 * Created by Ary on 04-Apr-16.
 */
public enum AccountType {
    SavingAccount,
    SpendingAccount
}
