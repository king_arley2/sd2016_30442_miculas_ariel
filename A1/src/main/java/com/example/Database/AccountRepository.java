package com.example.Database;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ary on 04-Apr-16.
 */
public interface AccountRepository extends JpaRepository<Account, Integer> {
}
