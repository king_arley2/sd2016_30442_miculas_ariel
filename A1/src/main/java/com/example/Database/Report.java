package com.example.Database;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Ary on 07-Apr-16.
 */
@Entity
public class Report {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer idReport;
    private Date dateOfCreation;
    @ManyToOne(optional = false)
    @JoinColumn(name = "sourceAccount", referencedColumnName = "idAccount")
    private Account sourceAccount;
    private Integer destinationAccountId;
    private Integer transferAmount;
    @NotBlank
    private String message;

    public Report()
    {
        this.dateOfCreation = new Date();
    }

    public Report(String message, Account sourceAccount) {
        this();
        this.message = message;
        this.sourceAccount = sourceAccount;
    }

    public Report(String message, Account sourceAccount, Integer destinationAccountId, Integer transferAmount) {
        this(message, sourceAccount);
        this.destinationAccountId = destinationAccountId;
        this.transferAmount = transferAmount;
    }

    public Integer getIdReport() {
        return idReport;
    }

    public void setIdReport(Integer idReport) {
        this.idReport = idReport;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public Integer getDestinationAccountId() {
        return destinationAccountId;
    }

    public void setDestinationAccount(Integer destinationAccountId) {
        this.destinationAccountId = destinationAccountId;
    }

    public Integer getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(Integer transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Report{" +
                "idReport=" + idReport +
                ", dateOfCreation=" + dateOfCreation +
                ", sourceAccount=" + sourceAccount +
                ", message='" + message + '\'' +
                ", destinationAccountId=" + destinationAccountId +
                ", transferAmount=" + transferAmount +
                '}';
    }
}
