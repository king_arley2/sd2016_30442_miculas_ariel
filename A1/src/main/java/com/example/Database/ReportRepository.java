package com.example.Database;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ary on 07-Apr-16.
 */
public interface ReportRepository extends JpaRepository<Report, Integer> {
}
