package com.example.Database;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Ary on 11-Apr-16.
 */
public class SecurityEmployee extends Employee implements UserDetails {

    public SecurityEmployee(Employee employee)
    {
        this.setIdEmployee(employee.getIdEmployee());
        this.setName(employee.getName());
        this.setPassword(employee.getPassword());
        this.setType(employee.getType());
        this.setUsername(employee.getUsername());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(this.getType().toString()));

        /*
        for (UserType type : UserType.values()) {
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(type.toString());
            authorities.add(authority);
        }*/
        return authorities;
    }
}
