package com.example.Database;

import org.hibernate.validator.constraints.NotBlank;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

/**
 * Created by Ary on 04-Apr-16.
 */
@Entity
public class Employee{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer idEmployee;
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z]+([a-zA-Z]|-|\\s)*$")
    private String name;
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9_-]+$")
    private String username;
    @NotBlank
    //@Pattern(regexp = "^[a-zA-Z0-9_!@#$%^&*()-]+$")
    private String password;
    private UserType type;

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "idEmployee=" + idEmployee +
                ", name='" + name + '\'' +
                ", username=" + username +
                ", password='" + password + '\'' +
                ", type=" + type +
                '}';
    }
}
