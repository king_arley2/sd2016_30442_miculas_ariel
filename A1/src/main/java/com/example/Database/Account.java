package com.example.Database;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Ary on 04-Apr-16.
 */
//Create/update/delete/view client account (account information: identification number, type, amount of money, date of creation).

@Entity
public class Account {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer idAccount;
    private AccountType type;
    @NotNull
    @Min(0)
    private Integer amount;
    private Date dateOfCreation;
    @ManyToOne(optional = false)
    @JoinColumn(name = "client", referencedColumnName = "idClient")
    private Client client;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sourceAccount")
    private Collection<Report> reports;

    public Account() {
        this.dateOfCreation = new Date();
        //this.type = AccountType.SpendingAccount;
    }

    public Collection<Report> getReports() {
        return reports;
    }

    public void setReports(Collection<Report> reports) {
        this.reports = reports;
    }

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "Account{" +
                "idAccount=" + idAccount +
                ", type=" + type +
                ", amount=" + amount +
                ", dateOfCreation=" + dateOfCreation +
                ", client=" + client +
                '}';
    }
}
