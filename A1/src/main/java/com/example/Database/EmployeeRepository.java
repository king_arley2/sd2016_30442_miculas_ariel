package com.example.Database;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Ary on 04-Apr-16.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    public List<Employee> findByName(String name);
    public List<Employee> findByUsername(String username);
}
