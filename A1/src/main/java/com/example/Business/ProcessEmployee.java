package com.example.Business;

import com.example.Database.Employee;
import com.example.Database.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Component
public class ProcessEmployee {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> findAll()
    {
        return employeeRepository.findAll();
    }

    public Employee findOne(Integer id)
    {
        return employeeRepository.findOne(id);
    }
    public List<Employee> findByUsername(String username)
    {
        return employeeRepository.findByUsername(username);
    }

    public void delete(Integer id)
    {
        employeeRepository.delete(id);
    }

    public void save(Employee employee)
    {
        employee.setPassword(new BCryptPasswordEncoder().encode(employee.getPassword()));
        employeeRepository.save(employee);
    }
}
