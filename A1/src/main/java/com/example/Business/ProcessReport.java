package com.example.Business;

import com.example.Database.Report;
import com.example.Database.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Component
public class ProcessReport {
    @Autowired
    private ReportRepository reportRepository;

    public List<Report> findAll()
    {
        return reportRepository.findAll();
    }

    public void save(Report report)
    {
        reportRepository.save(report);
    }
}
