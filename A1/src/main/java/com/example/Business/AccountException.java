package com.example.Business;

/**
 * Created by Ary on 06-Apr-16.
 */
public class AccountException extends Exception {
    public AccountException(String message)
    {
        super(message);
    }
}
