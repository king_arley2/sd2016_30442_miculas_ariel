package com.example.Business;

import com.example.Database.Client;
import com.example.Database.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ary on 07-Apr-16.
 */
@Component
public class ProcessClient {

    @Autowired
    ClientRepository clientRepository;
    public Client findOne(Integer idClient)
    {
        return clientRepository.findOne(idClient);
    }

    public List<Client> findAll()
    {
        return clientRepository.findAll();
    }

    public List<Client> findByPersonalNumericalCode(String pnc)
    {
        return clientRepository.findByPersonalNumericalCode(pnc);
    }

    public void updateClient(Integer clientId, Client client)
    {
        client.setIdClient(clientId);
        clientRepository.save(client);
        System.out.println("Updating client");
    }

    public void saveClient(Client client)
    {
        clientRepository.save(client);
        System.out.println("Saving client");
    }

    public void deleteClient(Integer clientId)
    {
        clientRepository.delete(clientId);
        System.out.println("Deleting client");
    }
}
