use strict;
use warnings;
use Storable;

our @genres=("Comedy", "Drama", "Non-fiction", "Realistic", "fiction", "Romance novel", "Satire", "Tragedy", "Tragicomedy", "Horror");

sub parseHtml
{
	do {my $elements = retrieve('objects'); return ($elements->[0], $elements->[1]);} if -f 'objects';
	
	my $filename = 'titles.in';
	open(my $fh, $filename) or die "Could not open file '$filename' $!";

	my $file = join "", <$fh>;
	my ($titles, $authors);
	@$titles = ($file =~ /(?:a title="([^"]+)")/g)[0..99];
	do {s/&#39;/'/g;} for @$titles;


	@$authors = ($file =~ /(?:<a class="authorName" itemprop="url" href="[^"]+"><span itemprop="name">([^<>]+))/g);
	do {s/&#39;/'/g;s/�/e/g;s/��/i/g;s/�/i/g;s/�/a/g;} for @$authors;
	
	store [$titles, $authors], 'objects';
	return ($titles, $authors);

}

sub genXml
{
	my $id = 1;
	my ($titles,$authors) = @_;
	print '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
	print "\n<books>\n";
	for (my $i = 0; $i < @$titles; $i++)
	{
		my $p = 10 + int(rand(100));
		my $q = int(rand(100));
		my $genreIndex = int(rand(@genres));
			# <title>Sacru si profran</title>
			# <author>Mircea Eliade</author>
			# <genre>Sf</genre>
			# <quantity>1</quantity>
			# <price>10</price>
		print "\t<book>\n";
		print "\t\t<id>$id</id>\n";
		print "\t\t<title>$titles->[$i]</title>\n";
		print "\t\t<author>$authors->[$i]</author>\n";
		print "\t\t<genre>$genres[$genreIndex]</genre>\n";
		print "\t\t<quantity>$q</quantity>\n";
		print "\t\t<price>$p</price>\n";
		print "\t</book>\n";
		print "\n";
		$id++;
		# print "$titles->[$i] --> $authors->[$i]\n";
	}
	print "</books>";
}


genXml(parseHtml())
 
 
 # print join "\n",@$authors;
 
  