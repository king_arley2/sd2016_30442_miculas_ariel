package com.example.data;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Ary on 17-Apr-16.
 */
@Component
public class BookRepository {
    private Books books;

    public Books getBooks() {
        return books;
    }

    public void setBooks(Books books) {
        this.books = books;
    }

    public BookRepository() {
        try {
            File file = new File("xml/books.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Books.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            books = (Books) jaxbUnmarshaller.unmarshal(file);
            books.getBookArrayList().sort((o1,o2) -> o1.getId() - o2.getId());
        } catch (JAXBException e) {
            e.printStackTrace();
            System.exit(1);
        }


        System.out.println("books: " + books);
    }

    public void saveBooksToXml()
    {
        File file = new File("xml/books.xml");
        try
        {
            JAXBContext jaxbContext = JAXBContext.newInstance(Books.class);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(books, file);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
    }
}
