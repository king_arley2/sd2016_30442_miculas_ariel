package com.example.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by Ary on 21-Apr-16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Sales {
    @XmlElement(name = "sale")
    private ArrayList<Sale> saleArrayList = new ArrayList<>();

    public ArrayList<Sale> getSaleArrayList() {
        return saleArrayList;
    }

    public void setSaleArrayList(ArrayList<Sale> sales) {
        this.saleArrayList = sales;
    }

    @Override
    public String toString() {
        return "Sales{" +
                "sales=" + saleArrayList +
                '}';
    }
}
