package com.example.data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * Created by Ary on 21-Apr-16.
 */
public class BookForm {
    @Pattern(regexp = "[a-zA-Z '?:.;*!0-9-]+")
    private String title;

    @Pattern(regexp = "[a-zA-Z' .-]+")
    private String author;

    @Pattern(regexp = "[a-zA-Z. -]+")
    private String genre;

    @Min(0)
    private Integer quantity;

    @Min(1)
    private Integer price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BookForm{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
