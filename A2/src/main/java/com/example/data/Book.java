package com.example.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by Ary on 16-Apr-16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {
    //(book information: title, author, genre, quantity, and price)
    private Integer id;
    private String title;
    private String author;
    private String genre;
    private Integer quantity;
    private Integer price;
    @XmlTransient
    Integer score;

    public Book() {
    }

    public Book(Integer id, BookForm bookForm) {
        this.id = id;
        updateBook(bookForm);
    }

    public void updateBook(BookForm bookForm)
    {
        this.title = bookForm.getTitle();
        this.author = bookForm.getAuthor();
        this.genre = bookForm.getGenre();
        this.quantity = bookForm.getQuantity();
        this.price = bookForm.getPrice();
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String toStringCsv()
    {
        return id
                + ","
                + title + ","
                + author + ","
                + genre + ","
                + price
                + "\n";
    }

}
