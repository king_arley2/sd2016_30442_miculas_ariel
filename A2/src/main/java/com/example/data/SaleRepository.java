package com.example.data;

import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Ary on 21-Apr-16.
 */
@Component
public class SaleRepository {
    private Sales sales;

    public SaleRepository()
    {
        try {
            File file = new File("xml/sales.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Sales.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            sales = (Sales) jaxbUnmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
            System.exit(1);
        }


        System.out.println("sales: " + sales);
    }

    public Sales getSales() {
        return sales;
    }

    public void setSales(Sales sales) {
        this.sales = sales;
    }

    public void saveSalesToXml()
    {
        File file = new File("xml/sales.xml");
        try
        {
            JAXBContext jaxbContext = JAXBContext.newInstance(Sales.class);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(sales, file);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
    }
}
