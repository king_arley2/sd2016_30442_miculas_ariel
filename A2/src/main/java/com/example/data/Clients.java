package com.example.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;

/**
 * Created by Ary on 16-Apr-16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Clients {
    @XmlElement(name = "client")
    private HashMap<String, Client> clientHashMap = new HashMap<String, Client>();

    public HashMap<String, Client> getClientHashMap() {
        return clientHashMap;
    }

    public void setClientHashMap(HashMap<String, Client> clientHashMap) {
        this.clientHashMap = clientHashMap;
    }

    @Override
    public String toString() {
        return "Clients{" +
                "clientHashMap=" + clientHashMap +
                '}';
    }
}
