package com.example.data;

import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Ary on 17-Apr-16.
 */
@Component
public class ClientRepository {
    private Clients clients = new Clients();

    public ClientRepository()
    {
        try {
            File file = new File("xml/clients.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Clients.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            clients = (Clients) jaxbUnmarshaller.unmarshal(file);
            System.out.println("clients: " + clients);
        } catch (JAXBException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public Clients getClients() {
        return clients;
    }

    public void setClients(Clients clients) {
        this.clients = clients;
    }

    public void saveClientsToXml()
    {
        File file = new File("xml/clients.xml");
        try
        {
            JAXBContext jaxbContext = JAXBContext.newInstance(Clients.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(clients, file);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
    }
}
