package com.example.data;

/**
 * Created by Ary on 17-Apr-16.
 */
public enum ReportType {
    PdfReport,
    CsvReport
}
