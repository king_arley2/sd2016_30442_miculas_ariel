package com.example.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ary on 16-Apr-16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Client {
    private String passwordHash;
    private ClientType type;
    private String name;
    private Integer salary;
    private Integer numberOfRestDaysLeft;

    public Client() {
    }

    public Client(String passwordHash, ClientType type, String name, Integer salary, Integer numberOfRestDaysLeft) {
        this.passwordHash = passwordHash;
        this.type = type;
        this.name = name;
        this.salary = salary;
        this.numberOfRestDaysLeft = numberOfRestDaysLeft;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public ClientType getType() {
        return type;
    }

    public void setType(ClientType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getNumberOfRestDaysLeft() {
        return numberOfRestDaysLeft;
    }

    public void setNumberOfRestDaysLeft(Integer numberOfRestDaysLeft) {
        this.numberOfRestDaysLeft = numberOfRestDaysLeft;
    }

    @Override
    public String toString() {
        return "Client{" +
                "passwordHash='" + passwordHash + '\'' +
                ", type=" + type +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", numberOfRestDaysLeft=" + numberOfRestDaysLeft +
                '}';
    }

}
