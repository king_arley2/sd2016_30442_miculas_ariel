package com.example.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ary on 21-Apr-16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Sale {
    private String username;
    private Integer bookId;
    private String bookTitle;
    private Integer amount;

    public Sale() {
    }

    public Sale(String username, Integer bookId, String bookTitle, Integer amount) {
        this.username = username;
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.amount = amount;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "username='" + username + '\'' +
                ", bookTitle='" + bookTitle + '\'' +
                ", amount=" + amount +
                '}';
    }
}
