package com.example.data;

/**
 * Created by Ary on 19-Apr-16.
 */
public class Report {
    private ReportType type;

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }
}
