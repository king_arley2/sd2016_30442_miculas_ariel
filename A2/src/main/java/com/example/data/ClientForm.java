package com.example.data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * Created by Ary on 20-Apr-16.
 */
public class ClientForm {
    @Pattern(regexp = "[a-zA-Z0-9_.-]+")
    private String username;

//    This regex will enforce these rules:
//
//    At least one upper case english letter
//    At least one lower case english letter
//    At least one digit
//    At least one special character
//    Minimum 8 in length
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%^&*()-]).{8,}$")
    private String password;

    @Pattern(regexp = "[a-zA-Z' .-]+")
    private String name;

    @Min(800)
    private Integer salary;

    @Min(21)
    @Max(31)
    private Integer numberOfRestDaysLeft;

    private String passwordHash;

    public ClientForm() {
    }

    public ClientForm(String username, Client client)
    {
        this.username = username;
        this.name = client.getName();
        this.salary = client.getSalary();
        this.numberOfRestDaysLeft = client.getNumberOfRestDaysLeft();
        this.passwordHash = client.getPasswordHash();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getNumberOfRestDaysLeft() {
        return numberOfRestDaysLeft;
    }

    public void setNumberOfRestDaysLeft(Integer numberOfRestDaysLeft) {
        this.numberOfRestDaysLeft = numberOfRestDaysLeft;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public String toString() {
        return "ClientForm{" +
                "username='" + username + '\'' +
                ", passwordHash='" + password + '\'' +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", numberOfRestDaysLeft=" + numberOfRestDaysLeft +
                '}';
    }
}
