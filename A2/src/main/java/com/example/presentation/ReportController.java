package com.example.presentation;

import com.example.business.ReportFactory;
import com.example.data.ReportType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Ary on 17-Apr-16.
 */
@Controller
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private ReportFactory reportFactory;

    @RequestMapping("/create/{type}")
    @ResponseBody
    public FileSystemResource generateReport(@PathVariable("type") ReportType type) {
        String path = reportFactory.getReportType(type).generateReport();
        return new FileSystemResource(path);
    }
}
