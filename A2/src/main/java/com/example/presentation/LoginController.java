package com.example.presentation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created by Ary on 16-Apr-16.
 */
@Controller
public class LoginController {
    @RequestMapping("/login")
    public String loginGet() {
        return "login";
    }

    @RequestMapping("/home")
    public String home() {
        return "home";
    }

    @RequestMapping("/")
    public String root()
    {
        return "redirect:/home";
    }

}
