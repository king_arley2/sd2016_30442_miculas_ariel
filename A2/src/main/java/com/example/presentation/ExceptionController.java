package com.example.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by Ary on 21-Apr-16.
 */
@Controller
public class ExceptionController {

    // for 403 access denied page
    @RequestMapping(value = "/permissionDenied", method = RequestMethod.GET)
    public String accesssDenied(Principal user, Model model) {
        String message;
        if (user != null) {
            message = "Hi " + user.getName()
                    + ", you do not have permission to access this page!";
        } else {
            message = "You do not have permission to access this page!";
        }
        model.addAttribute("errorMessage", message);
        return "exception";
    }

    // for 404 page not found
    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String pageNotFound(Principal user, Model model) {
        String message;
        message = "Page not found, contact the system administrator!";
        model.addAttribute("errorMessage", message);
        return "exception";
    }
}
