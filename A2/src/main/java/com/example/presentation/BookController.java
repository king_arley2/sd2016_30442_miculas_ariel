package com.example.presentation;

import com.example.business.BookException;
import com.example.business.BookProcessor;
import com.example.data.Book;
import com.example.data.BookForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by Ary on 16-Apr-16.
 */
@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookProcessor bookProcessor;
    
    @RequestMapping("/view")
    public String getBooks(Book book, Model model)
    {
        model.addAttribute("bookList", bookProcessor.getAllBooks());
        return "book/viewBooks";
    }


    @RequestMapping(value="/search", method = RequestMethod.GET)
    public String showFormSearchBook(Book book, Model model)
    {
        return "/book/viewBooks";
    }

    @RequestMapping(value="/search", method = RequestMethod.POST)
    public String searchBook(@Valid Book book, BindingResult bindingResult, Model model)
    {
        if (bindingResult.hasErrors())
        {
            return "/book/viewBooks";
        }

        model.addAttribute("bookList", bookProcessor.getBooksByPattern(book.getTitle()));
        return "book/viewBooks";
    }

    @RequestMapping(value = "/{bookId}", method = RequestMethod.GET)
    public String processViewBookDetails(@PathVariable("bookId") int bookId, BookForm bookForm, Model model)
    {
        Book currentBook = bookProcessor.getById(bookId);
        model.addAttribute("currentBook", currentBook);
        return "book/bookDetails";
    }

    @RequestMapping(value = "/{bookId}", method = RequestMethod.POST)
    public String viewBookDetails(@PathVariable("bookId") int bookId, @Valid BookForm bookForm, BindingResult bindingResult, Principal principal, Model model)
    {
        Book currentBook = bookProcessor.getById(bookId);
        model.addAttribute("currentBook", currentBook);

        if (bindingResult.hasErrors())
        {
            return "book/bookDetails";
        }

        try {
            bookProcessor.sellBook(bookId, bookForm.getQuantity(), principal.getName());
        } catch (BookException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "book/bookDetails";
        }
        return "book/bookDetails";
    }

    @PreAuthorize("hasAuthority('Admin')")
    @RequestMapping(value="new", method = RequestMethod.GET)
    public String showFormCreateNewBook(BookForm bookForm)
    {
        return "book/createBook";
    }

    @PreAuthorize("hasAuthority('Admin')")
    @RequestMapping(value="new", method = RequestMethod.POST)
    public String createNewBook(@Valid BookForm bookForm, BindingResult bindingResult, Model model)
    {
        if (bindingResult.hasErrors())
        {
            return "book/createBook";
        }
        int id = bookProcessor.saveBook(bookForm);
        return "redirect:/book/" + id;
    }


    @PreAuthorize("hasAuthority('Admin')")
    @RequestMapping(value = "/{bookId}/edit", method = RequestMethod.GET)
    public String editBookGet(@PathVariable("bookId") int bookId, BookForm bookForm, Model model)
    {
        Book book = bookProcessor.getById(bookId);
        model.addAttribute("bookForm", book);
        return "book/createBook";
    }

    @PreAuthorize("hasAuthority('Admin')")
    @RequestMapping(value = "/{bookId}/edit", method = RequestMethod.POST)
    public String editBookPost(@PathVariable("bookId") int bookId, @Valid BookForm bookForm,  BindingResult bindingResult, Model model)
    {
        Book book = bookProcessor.getById(bookId);

        if (bindingResult.hasErrors())
        {
            return "book/createBook";
        }
        bookProcessor.saveBook(bookId, bookForm);
        return "redirect:/book/" + bookId;
    }

    @PreAuthorize("hasAuthority('Admin')")
    @RequestMapping(value = "/{bookId}/delete", method = RequestMethod.GET)
    public String deleteBook(@PathVariable("bookId") int bookId, Model model)
    {
        bookProcessor.removeBook(bookId);
        return "redirect:/book/view";
    }
}
