package com.example.presentation;

import com.example.business.ClientException;
import com.example.business.ClientProcessor;
import com.example.data.Client;
import com.example.data.ClientForm;
import com.example.data.ClientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.HashMap;

/**
 * Created by Ary on 16-Apr-16.
 */
@Controller
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientProcessor clientProcessor;

    @RequestMapping("view")
    public String getClients(Model model)
    {
        HashMap<String, Client> hashMap = clientProcessor.getAllRegularClients();
        model.addAttribute("clientList", hashMap);
        return "client/viewClients";
    }

    @RequestMapping("/{clientUsername}")
    public String showClientInfo(@PathVariable("clientUsername") String clientUsername, Model model) {

        Client client = clientProcessor.getClient(clientUsername);
        if (client == null)
        {
            return "redirect:/404";
        }
        if (client.getType() == ClientType.Admin)
        {
            return "redirect:/permissionDenied";
        }
        model.addAttribute("client", client);
        model.addAttribute("username", clientUsername);
        return "client/clientDetails";
    }

    @RequestMapping(value="new", method = RequestMethod.GET)
    public String showFormCreateNewClient(ClientForm client)
    {
        return "client/createClient";
    }

    @RequestMapping(value="new", method = RequestMethod.POST)
    public String createNewClient(@Valid ClientForm client, BindingResult bindingResult, Model model)
    {
        if (bindingResult.hasErrors())
        {
            return "client/createClient";
        }

        try {
            clientProcessor.save(client);
        } catch (ClientException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "client/createClient";
        }

        return "redirect:/client/" + client.getUsername();
    }

    @RequestMapping(value = "/{clientUsername}/edit", method = RequestMethod.GET)
    public String processUpdateClientForm(@PathVariable("clientUsername") String clientUsername, Model model) {
        if (null == clientProcessor.getClient(clientUsername))
        {
            return "redirect:/404";
        }
        model.addAttribute("clientForm", clientProcessor.getClientForm(clientUsername));
        return "client/editClient";
    }

    @RequestMapping(value="/{clientUsername}/edit", method = RequestMethod.POST)
    public String updateClient(@PathVariable("clientUsername") String clientUsername,
                               @Valid ClientForm client,
                               BindingResult bindingResult,
                               Model model)
    {
        if (null == clientProcessor.getClient(clientUsername))
        {
            return "redirect:/404";
        }

        if (bindingResult.hasErrors())
        {
            return "client/editClient";
        }

        try {
            clientProcessor.update(clientUsername, client);
        } catch (ClientException e) {
            return "client/editClient";
        }

        return "redirect:/client/" + clientUsername;
    }

    @RequestMapping(value = "/{clientUsername}/delete")
    public String processDeleteClientForm(@PathVariable("clientUsername") String clientUsername) {
        clientProcessor.delete(clientUsername);
        return "redirect:/client/view";
    }
}
