package com.example.presentation;

import com.example.business.SaleProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Ary on 21-Apr-16.
 */
@Controller
@RequestMapping("/sale")
public class SaleController {
    @Autowired
    private SaleProcessor saleProcessor;
    @RequestMapping("/view")
    public String viewSaleReport(Model model)
    {
        model.addAttribute("saleList", saleProcessor.getAllSales());
        return "sale/viewSales";
    }
}
