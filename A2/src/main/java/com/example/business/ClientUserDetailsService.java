package com.example.business;

import com.example.data.Client;
import com.example.data.SecurityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by Ary on 16-Apr-16.
 */
@Component
public class ClientUserDetailsService implements UserDetailsService{
    @Autowired
    ClientProcessor clientProcessor;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Client client = clientProcessor.getClient(username);
        if (client == null)
        {
            throw new UsernameNotFoundException("UserName "+username+" not found");
        }
        SecurityClient securityClient = new SecurityClient(client, username);
        return securityClient;
    }
}
