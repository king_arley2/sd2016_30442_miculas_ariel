package com.example.business;

/**
 * Created by Ary on 18-Apr-16.
 */
public class FuzzyMatch {

    final static private int adjacency_bonus = 5;              // bonus for adjacent matches
    final static private int separator_bonus = 10;             // bonus if match occurs after a separator
    final static private int camel_bonus = 10;                 // bonus if match is uppercase and prev is lower

    final static private int leading_letter_penalty = -3;      // penalty applied for every letter in str before the first match
    final static private int max_leading_letter_penalty = -9;  // maximum penalty for leading letters
    final static private int unmatched_letter_penalty = -1;    // penalty for every letter that doesn't matter

    // Returns true if each character in pattern is found sequentially within str
    static public Boolean patternMatch(String pattern, String str)
    {
        int i = 0, j = 0;
        int patternLength = pattern.length();
        int strLength = str.length();

        while (i < patternLength && j < strLength)
        {
            if (Character.toLowerCase(pattern.charAt(i)) == Character.toLowerCase(str.charAt(j)))
            {
                i++;
            }
            j++;
        }
        return i == patternLength;
    }

    // Returns the score of the matching
    // Score value has no intrinsic meaning. Range varies with pattern.
    // Can only compare scores with same search pattern.
    static public Integer fuzzyMatch(String pattern, String str)
    {
        // Loop variables
        int score = 0;
        Integer patternIter = 0;
        Integer patternLength = pattern.length();

        Integer strIter = 0;
        Integer strLength = str.length();

        Boolean prevMatched = false;
        Boolean prevLower = false;
        Boolean prevSeparator = true;                  // true so if first letter match gets separator bonus

        // Use "best" matched letter if multiple string letters match the pattern
        Integer bestLetter = null;
        Integer bestLetterScore = 0;

        // Loop over strings
        while (strIter < strLength)
        {
            Character patternLetter = patternIter < patternLength ? pattern.charAt(patternIter) : 0;
            Character strLetter = str.charAt(strIter);

            Boolean nextMatch =
                    patternIter < patternLength
                    && Character.toLowerCase(patternLetter) == Character.toLowerCase(strLetter);
            Boolean rematch =
                    (bestLetter != null)
                    && (Character.toLowerCase(str.charAt(bestLetter)) == Character.toLowerCase(strLetter));

            Boolean advanced = nextMatch && (bestLetter != null);
            Boolean patternRepeat =
                    bestLetter != null
                    && patternIter < patternLength
                    && Character.toLowerCase(str.charAt(bestLetter)) == Character.toLowerCase(patternLetter);

            if (advanced || patternRepeat) {
                score += bestLetterScore;
                bestLetter = null;
                bestLetterScore = 0;
            }

            if (nextMatch || rematch) {
                int newScore = 0;

                // Apply penalty for each letter before the first pattern match
                // Note: std::max because penalties are negative values. So max is smallest penalty.
                if (patternIter == 0) {
                    int count = strIter;
                    int penalty = leading_letter_penalty * count;
                    if (penalty < max_leading_letter_penalty)
                    {
                        penalty = max_leading_letter_penalty;
                    }

                    score += penalty;
                }

                // Apply bonus for consecutive bonuses
                if (prevMatched) {
                    newScore += adjacency_bonus;
                }

                // Apply bonus for matches after a separator
                if (prevSeparator) {
                    newScore += separator_bonus;
                }

                // Apply bonus across camel case boundaries
                if (prevLower && Character.isUpperCase(strLetter)) {
                    newScore += camel_bonus;
                }

                // Update pattern iter IFF the next pattern letter was matched
                if (nextMatch) {
                    ++patternIter;
                }

                // Update best letter in str which may be for a "next" letter or a rematch
                if (newScore >= bestLetterScore) {
                    // Apply penalty for now skipped letter
                    if (bestLetter != null) {
                        score += unmatched_letter_penalty;
                    }

                    bestLetter = strIter;
                    bestLetterScore = newScore;
                }

                prevMatched = true;
            }
            else {
                score += unmatched_letter_penalty;
                prevMatched = false;
            }

            // Separators should be more easily defined
            prevLower = Character.isLowerCase(strLetter);
            prevSeparator = strLetter == '_' || strLetter == ' ';

            ++strIter;
        }

        // Apply score for last match
        if (bestLetter != null) {
            score += bestLetterScore;
        }

        // Did not match full pattern
        if (patternIter < patternLength) {
            return 0;
        }

        return score;
    }
}
