package com.example.business;

import com.example.data.Book;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ary on 17-Apr-16.
 */
public class ReportCsv implements ReportInterface{
    BookProcessor bookProcessor;
    public ReportCsv(BookProcessor bookProcessor) {
        this.bookProcessor = bookProcessor;
    }

    public String generateReport()
    {
        final String path = "reports/stock.csv";
        int id = 1;
        try {
            FileWriter fileWriter = new FileWriter(path);

            ArrayList<Book> list =  bookProcessor.getAllBooks();
            for (Book book : list) {
                if (book.getQuantity() == 0)
                {
                    fileWriter.append(book.toStringCsv());
                    id += 1;
                }
            }
            fileWriter.flush();
            fileWriter.close();
            return path;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
