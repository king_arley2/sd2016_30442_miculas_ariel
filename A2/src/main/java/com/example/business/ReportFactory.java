package com.example.business;

import com.example.data.ReportType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Ary on 17-Apr-16.
 */
@Component
public class ReportFactory {
    @Autowired
    private BookProcessor bookProcessor;

    public ReportInterface getReportType(ReportType type)
    {
        if (type.equals(ReportType.CsvReport))
        {
            return new ReportCsv(bookProcessor);
        }
        else if (type.equals(ReportType.PdfReport))
        {
            return new ReportPdf(bookProcessor);
        }
        else
        {
            return null;
        }
    }
}
