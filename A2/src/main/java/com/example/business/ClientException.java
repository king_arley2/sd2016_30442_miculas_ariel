package com.example.business;

/**
 * Created by Ary on 20-Apr-16.
 */
public class ClientException extends Exception {
    public ClientException(String message)
    {
        super(message);
    }
}
