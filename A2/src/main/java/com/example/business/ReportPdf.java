package com.example.business;

import com.example.data.Book;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by Ary on 17-Apr-16.
 */
public class ReportPdf implements ReportInterface{
    BookProcessor bookProcessor;
    public ReportPdf(BookProcessor bookProcessor) {
        this.bookProcessor = bookProcessor;
    }

    public String generateReport() {

        Document document = new Document();
        final String path = "reports/stock.pdf";
        Boolean useList = false;
        Boolean useTable = true;

        ArrayList<Book> bookList = bookProcessor.getAllBooks();
        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream(path));

            document.open();

            if (useList) {
                List orderedList = new List(List.ORDERED);
                for (Book book : bookList) {
                    if (book.getQuantity() == 0) {
                        orderedList.add(new ListItem(book.getTitle()));
                    }
                }

                document.add(orderedList);
            }
            else if (useTable) {
                PdfPTable table = new PdfPTable(5);

                float[] columnWidths = {1f, 5f, 3f, 3f, 1f};

                table.setWidths(columnWidths);
                table.addCell(new Paragraph("Id"));
                table.addCell(new Paragraph("Title"));
                table.addCell(new Paragraph("Author"));
                table.addCell(new Paragraph("Genre"));
                table.addCell(new Paragraph("Price"));

                for (Book book : bookList) {
                    if (book.getQuantity() == 0) {
                        table.addCell(new Paragraph(book.getId().toString()));
                        table.addCell(new Paragraph(book.getTitle()));
                        table.addCell(new Paragraph(book.getAuthor()));
                        table.addCell(new Paragraph(book.getGenre()));
                        table.addCell(new Paragraph(book.getPrice().toString()));
                    }

                }
                document.add(table);
            }
            document.close();
            return path;
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
