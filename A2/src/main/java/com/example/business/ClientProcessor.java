package com.example.business;

import com.example.data.Client;
import com.example.data.ClientForm;
import com.example.data.ClientRepository;
import com.example.data.ClientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by Ary on 16-Apr-16.
 */
@Component
public class ClientProcessor {

    @Autowired
    private ClientRepository clientRepository;

    public Client getClient(String username)
    {
        return clientRepository.getClients().getClientHashMap().get(username);
    }

    public ClientForm getClientForm(String username)
    {
        return new ClientForm(username, clientRepository.getClients().getClientHashMap().get(username));
    }

    public HashMap<String,Client> getAllClients(){
        return clientRepository.getClients().getClientHashMap();
    }

    public HashMap<String,Client> getAllRegularClients(){
        HashMap<String, Client> regularClients = new HashMap<>();
        HashMap<String, Client> hashMap = clientRepository.getClients().getClientHashMap();
        for (String key : hashMap.keySet()) {
            if (hashMap.get(key).getType() == ClientType.Regular)
            {
                regularClients.put(key, hashMap.get(key));
            }
        }

        return regularClients;
    }

    public void save(ClientForm clientForm) throws ClientException {
        Boolean exists = clientRepository.getClients().getClientHashMap().containsKey(clientForm.getUsername());

        if (exists)
        {
            throw new ClientException("Username already in use");
        }
        Client client = new Client(
                new BCryptPasswordEncoder().encode(clientForm.getPassword()),
                ClientType.Regular,
                clientForm.getName(),
                clientForm.getSalary(),
                clientForm.getNumberOfRestDaysLeft()
        );
        clientRepository.getClients().getClientHashMap().put(clientForm.getUsername(), client);
        clientRepository.saveClientsToXml();
    }

    public void update(String username, ClientForm clientForm) throws ClientException {
        Client client = clientRepository.getClients().getClientHashMap().get(username);
        if (client == null)
        {
            throw new ClientException("Username not found!");
        }
        client.setName(clientForm.getName());
        client.setSalary(clientForm.getSalary());
        client.setNumberOfRestDaysLeft(clientForm.getNumberOfRestDaysLeft());
        clientRepository.saveClientsToXml();
    }

    public void delete(String username)
    {
        clientRepository.getClients().getClientHashMap().remove(username);
        clientRepository.saveClientsToXml();
    }

    public void test()
    {
        Client client = new Client();
        client.setName("Ariel");
        client.setNumberOfRestDaysLeft(21);
        client.setPasswordHash(new BCryptPasswordEncoder().encode("user"));
        client.setSalary(1200);
        client.setType(ClientType.Admin);
        clientRepository.getClients().getClientHashMap().put("user", client);
        clientRepository.saveClientsToXml();
    }
}
