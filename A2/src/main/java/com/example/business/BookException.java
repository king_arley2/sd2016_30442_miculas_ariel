package com.example.business;

/**
 * Created by Ary on 21-Apr-16.
 */
public class BookException extends Exception {
    public BookException(String message) {
        super(message);
    }
}
