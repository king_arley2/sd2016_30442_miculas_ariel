package com.example.business;

import com.example.data.Book;
import com.example.data.BookForm;
import com.example.data.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;

/**
 * Created by Ary on 16-Apr-16.
 */
@Component
public class BookProcessor {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private SaleProcessor saleProcessor;

    public void sellBook(Integer id, Integer amount, String username) throws BookException {
        Book book = getById(id);
        if (amount > book.getQuantity())
        {
            throw new BookException("Not enough books in stock!");
        }
        if (amount == 0)
        {
            throw new BookException("Warning: Selling 0 books has no effect!");
        }
        book.setQuantity(book.getQuantity() - amount);
        saleProcessor.addSale(username, book.getId(), book.getTitle(), amount);
        bookRepository.saveBooksToXml();
    }

    public void removeBook(Integer id) {
        int i = 0;
        ArrayList<Book> list = bookRepository.getBooks().getBookArrayList();
        for (i = 0; i < list.size(); i++) {
            if(list.get(i).getId().equals(id))
            {
                list.remove(i);
            }
        }
        bookRepository.saveBooksToXml();
    }

    public ArrayList<Book> getAllBooks()
    {
        return bookRepository.getBooks().getBookArrayList();
    }

    public Book getById(Integer id)
    {
        ArrayList<Book> list = new ArrayList<Book>(bookRepository.getBooks().getBookArrayList());

        for (Book book : list) {
            if (book.getId().equals(id))
            {
                return book;
            }
        }
        return null;
    }

    public Integer saveBook(BookForm bookForm)
    {
        ArrayList<Book> books = bookRepository.getBooks().getBookArrayList();
        int lastIndex = books.size() - 1;
        int newId = books.get(lastIndex).getId() + 1;
        books.add(new Book(newId, bookForm));
        bookRepository.saveBooksToXml();
        return newId;
    }

    public void saveBook(Integer id, BookForm bookForm)
    {
        Book book = getById(id);
        book.updateBook(bookForm);
        bookRepository.saveBooksToXml();
    }

    public ArrayList<Book> getBooksByPattern(String patternToMatch)
    {
        Boolean fuzzyMatch = true;
        ArrayList<Book> allBooks = new ArrayList<Book>(bookRepository.getBooks().getBookArrayList());
        if (patternToMatch.isEmpty())
        {
            return allBooks;
        }

        if (!fuzzyMatch) {
            Pattern pat = Pattern.compile(Pattern.quote(patternToMatch), Pattern.CASE_INSENSITIVE);
            allBooks.removeIf(s -> !pat.matcher(s.getTitle()).find());
            return allBooks;
        }
        else {
            ArrayList<Book> matchedBooks = new ArrayList<Book>();
            int score = 0;
            for (Book book : allBooks) {
                score = FuzzyMatch.fuzzyMatch(patternToMatch, book.getTitle());
                if (score > 0) {
                    System.out.println(book.getTitle() + "->" + score);
                    book.setScore(score);
                    matchedBooks.add(book);
                }
                else {
                    score = FuzzyMatch.fuzzyMatch(patternToMatch, book.getAuthor());
                    if (score > 0) {
                        book.setScore(score);
                        matchedBooks.add(book);
                    }
                    else
                    {
                        score = FuzzyMatch.fuzzyMatch(patternToMatch, book.getGenre());
                        if (score > 0)
                        {
                            book.setScore(score);
                            matchedBooks.add(book);
                        }
                    }
                }
            }
            Collections.sort(matchedBooks, (o1, o2) -> o2.getScore() - o1.getScore());
            return matchedBooks;
        }
    }

    @Override
    public String toString() {
        return "BookProcessor{" +
                "books=" + bookRepository.getBooks() +
                '}';
    }
}
