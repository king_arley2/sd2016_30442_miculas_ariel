package com.example.business;

import com.example.data.Sale;
import com.example.data.SaleRepository;
import com.example.data.Sales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by Ary on 21-Apr-16.
 */
@Component
public class SaleProcessor {
    @Autowired
    private SaleRepository saleRepository;

    public void addSale(String username, Integer id, String title, Integer amount)
    {
        saleRepository.getSales().getSaleArrayList().add(new Sale(username, id, title, amount));
        saleRepository.saveSalesToXml();
    }

    public ArrayList<Sale> getAllSales()
    {
        return saleRepository.getSales().getSaleArrayList();
    }
}
