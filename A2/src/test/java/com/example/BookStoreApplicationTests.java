package com.example;

import com.example.business.BookException;
import com.example.business.BookProcessor;
import com.example.business.FuzzyMatch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import com.example.data.Book;

import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BookStoreApplication.class)
@WebAppConfiguration
public class BookStoreApplicationTests {

	@Autowired
	private BookProcessor bookProcessor;

	@Test
	public void contextLoads() {
	}

	@Test
	public void checkPatternMatching()
	{
		int score = 0;
		score = FuzzyMatch.fuzzyMatch("tdkr", "The dark knight rises");
		assert(score > 0);
		score = FuzzyMatch.fuzzyMatch("aam", "Ana are mere");
		assert(score > 0);
		score = FuzzyMatch.fuzzyMatch("ana", "This is great");
		assert(score == 0);
	}

	@Test
	public void checkSale()
	{
		int i = 0;
		int amount = 0;
		for (i = 1; i <= bookProcessor.getAllBooks().size(); i++) {
			Book book = bookProcessor.getById(i);
			if (book.getQuantity() > 0) {
				amount = ThreadLocalRandom.current().nextInt(1, book.getQuantity() + 1);


				int initialQuantity = book.getQuantity();

				try {
					bookProcessor.sellBook(i, amount, "generic_user");
				} catch (BookException e) {
					e.printStackTrace();
				}
				int finalQuantity = book.getQuantity();
				assertEquals(finalQuantity + amount, initialQuantity);
			}
		}
	}

	@Test(expected = BookException.class)
	public void checkSale2() throws BookException {
		int amount = 0;
		int id = ThreadLocalRandom.current().nextInt(1, bookProcessor.getAllBooks().size() + 1);

		Book book = bookProcessor.getById(id);
		amount = ThreadLocalRandom.current().nextInt(book.getQuantity() + 2, book.getQuantity() + 100);

		bookProcessor.sellBook(id, amount, "generic_user");
	}

	@Test(expected = BookException.class)
	public void checkSale3() throws BookException {
		int amount = 0;
		int id = ThreadLocalRandom.current().nextInt(1, bookProcessor.getAllBooks().size() + 1);

		Book book = bookProcessor.getById(id);

		bookProcessor.sellBook(id, amount, "generic_user");
	}
}
